import React, { Component } from 'react';
import { StyleSheet, Text, View, YellowBox, AsyncStorage } from 'react-native';
import { Provider } from 'react-redux';
import store from './src/store/Store';
import { AppLoading } from 'expo';
import _ from 'lodash';
import { Provider as PaperProvider, Theme } from 'react-native-paper';
import { AppContainerSS, AppContainerCS } from './src/Rutas';
import Firebase from './src/Firebase';
import 'firebase/auth';
import { Tema, TemaDark, Http } from './src/Config';
// import Cargando from './src/componentes/Cargando'

// export default function App() {
class App extends Component {
  state = {
    isReady: false,
    sesion: false,
  };

  constructor(props) {
    super(props);
    YellowBox.ignoreWarnings(['Setting a timer']);
    const _console = _.clone(console);
    console.warn = message => {
      if (message.indexOf('Setting a timer') <= -1) {
        _console.warn(message);
      }
    };
  }

  componentDidMount() {
    // SplashScreen.preventAutoHide();
    // console.log(this.props)
  }

  cargarApp = async () => {
    return new Promise<void>((resolve, reject) => {
      // const unsubscribe = Firebase.auth().onAuthStateChanged(user => {
      //   unsubscribe();
      //   if (user) this.setState({ sesion: true })
      //   resolve();
      // }, reject);
      AsyncStorage.getItem('rt')
        .then((value) => {
          // console.log(value);
          
          Http.post('user/session', { remember_token: value })
            .then((res) => {
              // console.log(res)
              if(!res.error) this.setState({ sesion: true })
              resolve();
            })
            .catch(reject)
        })
    });
  }

  appCargada = () => {
    console.log('app cargada');
    return this.setState({ isReady: true })
  }

  render() {
    // return (
    //   <PaperProvider
    //     theme={Tema}
    //   >
    //     <Provider store={store}>
    //       <View style={{ flex: 1 }}>
    //         <Cargando/>
    //       </View>
    //     </Provider>
    //   </PaperProvider>
    // )
    if (!this.state.isReady) {
      return (
        <AppLoading
          startAsync={this.cargarApp}
          onFinish={this.appCargada}
          // onFinish={() => this.setState({ isReady: true })}
          onError={console.error}
          autoHideSplash={true}
        />
      );
    }

    if (this.state.sesion)
      return (
        <PaperProvider
          theme={Tema}
        >
          <Provider store={store}>
            <View style={{ flex: 1 }}>
              <AppContainerCS style={{ flex: 1, backgroundColor: '#000' }} />
            </View>
          </Provider>
        </PaperProvider>
      )

    return (
      <PaperProvider
        theme={Tema}
      >
        <Provider store={store}>
          <View style={{ flex: 1 }}>
            <AppContainerSS style={{ flex: 1, backgroundColor: '#000' }} />
            {/* <AppContainer style={{ flex: 1, backgroundColor: '#000' }} /> */}
          </View>
        </Provider>
      </PaperProvider>
    );
  }
}

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
