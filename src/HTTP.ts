

export default class Http {
    private method;
    private url;

    constructor(method, url) {
        this.method = method;
        this.url = url;
    }

    cors() {
        var xhr = new XMLHttpRequest();
        if ("withCredentials" in xhr) {
            // XHR for Chrome/Firefox/Opera/Safari.
            xhr.open(this.method, this.url, true);
        } else {
            // CORS not supported.
            xhr = null;
        }
        return xhr;
    }
}