import { DefaultTheme, Theme } from 'react-native-paper';

export const Tema: Theme = {
  ...DefaultTheme,
  roundness: 2,
  dark: false,
  colors: {
    ...DefaultTheme.colors,
    primary: '#3d72b8',
    background: '#fafafa',
    surface: '#000',
    accent: '#99a8ad',
    error: '#c72e2e',
    text: '#1c1c1c',
    disabled: '#b4cacf',
    placeholder: '#3d72b8',
    backdrop: '#e9ecf0',
  },
};

export const TemaDark: Theme = {
  ...DefaultTheme,
  roundness: 2,
  dark: true,
  colors: {
    ...DefaultTheme.colors,
    primary: '#3d72b8',
    background: '#383838',
    surface: '#000',
    accent: '#99a8ad',
    error: '#c72e2e',
    text: '#f7f7f7',
    disabled: '#b4cacf',
    placeholder: '#3d72b8',
    backdrop: '#000',
  },
};

export const Facebook = {
  id: '1017644328587872',
}

import axios from 'axios';

const eeUrl = 'https://app.estudiantesembajadores.com/app-ee/public/';
interface Response {
  error: boolean,
  data: any,
  errorCode?: number,
  errorMessage?: string,
}
export class Http {
  // api = 'https://app.estudiantesembajadores.com/app-ee/public/';

  static async get(url) {
    return (await axios.get(eeUrl + url)).data;
  }

  static async post(url, data) {
    return (await axios.post(
      eeUrl + url,
      data
    )).data;
  }
}