import * as firebase from 'firebase/app'
// import 'firebase/analytics'

firebase.initializeApp({
  apiKey: "AIzaSyCCPqyeWyiJhydL9Lk66GhK-MX4iZpKg3M",
  authDomain: "estudiantesembajadores-app.firebaseapp.com",
  databaseURL: "https://estudiantesembajadores-app.firebaseio.com",
  projectId: "estudiantesembajadores-app",
  storageBucket: "estudiantesembajadores-app.appspot.com",
  messagingSenderId: "570279247430",
  appId: "1:570279247430:web:b87f25212fde9316cd1447",
  measurementId: "G-NTKT66LFPE"
});

export default firebase;