import React, { Component } from "react";
import { Image, Text } from 'react-native';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
// import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { Tema } from './Config'
import Icon from 'react-native-vector-icons/FontAwesome';

import Registro from './componentes/Registro';
import Login from './componentes/Login';
import Perfil from './componentes/Perfil';
// import Inicio from './componentes/Inicio';
// import Principal from './componentes/Principal';

import Noticias from './componentes/Inicio/Noticias/Noticias';
import Mundo from './componentes/Inicio/Mundo/Mundo';
import Comentarios from './componentes/Inicio/Mundo/Comentarios';
import Crear, { Publicar } from './componentes/Inicio/Mundo/Crear';
import Media from './componentes/Media/Media';
import Notificaciones from './componentes/Inicio/Notificaciones/Notificaciones';
import Mensajes from './componentes/Inicio/Mensajes/Mensajes';
import Chat from './componentes/Inicio/Mensajes/Chat';

import Programas from './componentes/Programas';
import Tienda from './componentes/Tienda/Tienda';
import Ayuda from './componentes/Ayuda';

import Configuracion from './componentes/Configuracion/Configuracion';
import Datos from './componentes/Configuracion/Datos';


import { Drawer } from './componentes/Drawer';
import HeaderEE from './componentes/Header';
import Boveda from "./componentes/Configuracion/Boveda";
import NotificacionesConfig from "./componentes/Configuracion/Notificaciones";
import Orden from "./componentes/Tienda/Orden";
import Pago from "./componentes/Tienda/Pago";
import Pagos from "./componentes/Configuracion/Pagos";




const LoginStackNavigator = createStackNavigator({
  Login: { screen: Login },
  Registro: { screen: Registro },
});

const MundoStackNavigator = createStackNavigator({
  Mundo: { screen: Mundo, navigationOptions: { header: <HeaderEE /> } },
  Crear: { screen: Crear, navigationOptions: { title: 'Nueva publicación', } },
  Media: { screen: Media, navigationOptions: { title: 'Seleccionar' } },
  Comentarios: { screen: Comentarios, navigationOptions: { title: 'Comentarios' } },
});

const ConfigStackNavigator = createStackNavigator({
  Configuracion: { screen: Configuracion, navigationOptions: { title: 'Configuración' } },
  Pagos: { screen: Pagos, navigationOptions: { title: 'Pagos' } },
  Datos: { screen: Datos, navigationOptions: { title: 'Datos' } },
  Boveda: { screen: Boveda, navigationOptions: { title: 'Bóveda' } },
  NotificacionesConfig: { screen: NotificacionesConfig, navigationOptions: { title: 'Notificaciones' } },
});

const MensajesStackNavigator = createStackNavigator({
  Mensajes: { screen: Mensajes, navigationOptions: { header: <HeaderEE /> } },
  Chat: { screen: Chat, navigationOptions: { title: 'Chat' } },
});

const PerfilStackNavigator = createStackNavigator({
  Perfil: { screen: Perfil, navigationOptions: { header: <HeaderEE /> } },
  Media: { screen: Media, navigationOptions: { title: 'Seleccionar' } },
});

const TiendaStackNavigator = createStackNavigator({
  Tienda: { screen: Tienda, navigationOptions: { header: <HeaderEE /> } },
  Pago: { screen: Pago, navigationOptions: { title: 'Pagar' } },
  Orden: { screen: Orden, navigationOptions: { title: 'Confirmar orden' } },
});

const InicioTabNavigator = createMaterialBottomTabNavigator({
  Embajadores: {
    screen: MundoStackNavigator, navigationOptions: {
      headerTransparent: true,
      tabBarIcon: ({ tintColor }) => (
        <Image
          source={{ uri: 'https://i.ibb.co/yhzjHHN/Logo-EE-Azul-S.png' }}
          style={{ width: 24, height: 24, tintColor: tintColor }}
        />)
    }
  },
  Noticias: {
    screen: Noticias, navigationOptions: {
      headerTransparent: true,
      tabBarIcon: ({ tintColor }) => (<Icon color={tintColor} size={24} name={'globe'} />)
    }
  },
  Mensajes: {
    screen: MensajesStackNavigator, navigationOptions: {
      headerTransparent: true,
      tabBarIcon: ({ tintColor }) => (<Icon color={tintColor} size={24} name={'envelope'} />)
    }
  },
  Notificaciones: {
    screen: Notificaciones, navigationOptions: {
      headerTransparent: true,
      tabBarIcon: ({ tintColor }) => (<Icon color={tintColor} size={24} name={'bell'} />)
    }
  },
},
  {
    initialRouteName: 'Embajadores',
    activeColor: '#fff',
    inactiveColor: '#a4bcdb',
  });

const AppDrawerNavigator = createDrawerNavigator({
  Tienda: {
    screen: TiendaStackNavigator,
    navigationOptions: {
      drawerIcon: () => (<Icon name="shopping-cart" size={24} color={Tema.colors.primary} />)
    },
  },
  Inicio: {
    screen: InicioTabNavigator,
    navigationOptions: {
      drawerIcon: () => (<Icon name="home" size={24} color={Tema.colors.primary} />)
    },
  },
  Perfil: {
    screen: PerfilStackNavigator,
    navigationOptions: {
      drawerLabel: () => (null)
    },
  },
  Programas: {
    screen: Programas,
    navigationOptions: {
      drawerIcon: () => (<Icon name="plane" size={24} color={Tema.colors.primary} />)
    },
  },
  Configuración: {
    screen: ConfigStackNavigator,
    navigationOptions: {
      drawerIcon: () => (<Icon name="cog" size={24} color={Tema.colors.primary} />)
    },
  },
  Ayuda: {
    screen: Ayuda,
    navigationOptions: {
      drawerIcon: () => (<Icon name="question-circle" size={24} color={Tema.colors.primary} />)
    },
  },
}, {
  contentComponent: props => (<Drawer {...props} />),
  contentOptions: {
    activeTintColor: Tema.colors.primary,
    inactiveTintColor: '#6e6e6e',
    labelStyle: { fontSize: 20 },
  }
});

export const AppSwitchNavigator = createSwitchNavigator({
  Iniciar: { screen: LoginStackNavigator },
  Principal: { screen: AppDrawerNavigator },
});

export const AppSwitchNavigatorCS = createSwitchNavigator({
  Principal: { screen: AppDrawerNavigator },
  Iniciar: { screen: LoginStackNavigator },
});
export const AppContainerSS = createAppContainer(AppSwitchNavigator);
export const AppContainerCS = createAppContainer(AppSwitchNavigatorCS);