import { SET_USER, SET_USER_BYL, SET_SALES, SET_LOAD } from './Types';

const initialState = {
  user: {
    id: 'a',
    byl: {
      sales: {}
    }
  },
  sales: [],
  loading: false,
}

// async function getUser(state) {
// }

function reducer(state = initialState, action) {
  switch (action.type) {
    case SET_USER:
      return { ...state, loading: false, user: { ...action.value, byl: {...state.user.byl} } }
    case SET_USER_BYL:
      return { ...state, loading: false, user: { ...state.user, byl: action.value } }
    case SET_SALES:
      return { ...state, loading: false, sales: action.value }
    case SET_LOAD:
      return { ...state, loading: action.value }
    default:
      return state;
  }
}

export default reducer;