import {
  GET_USER,
  REQUEST_USER,
  RECEIVE_USER,
  SET_USER,
  SET_USER_BYL,
  SET_SALES,
  SET_LOAD
} from './Types';
import { AsyncStorage } from 'react-native';
import Firebase from '../Firebase';
import ByL from '../ByL';
import { Http } from '../Config';

export const setUser = (user) => {
  return {
    type: SET_USER,
    value: user
  }
}

export const setUserByL = (user) => {
  return {
    type: SET_USER_BYL,
    value: user
  }
}

export const setSales = (sales) => {
  return {
    type: SET_SALES,
    value: sales
  }
}

export const loading = (load) => {
  // console.log('set load a ', load)
  return {
    type: SET_LOAD,
    value: load
  }
}

export const getUser = (UID) => {
  // var UID = 'MoI2977NKpOVswoyscQzB8Xnm4t2';
  return (dispatch) => {
    // Firebase.firestore()
    //   .collection('users')
    //   .doc(UID)
    //   .onSnapshot(next => {
    //     // console.log(next.data())
    //     dispatch(setUser(next.data()))
    //   })
    Http.post('user', { rt: UID })
      .then((res) => {
        // console.log(res)
        dispatch(setUser(res.data))
      })
  }
}

export const saveUser = (rt, data) => {
  return (dispatch) => {
    Http.post('user/save', { rt: rt, ...data })
      .then((res) => {
        dispatch(setUser(res.data))
      })
  }
}

export const getUserByL = (rt) => {
  return (dispatch) => {
    dispatch(loading(true))
    // ByL.Datos(email)
    //   .then((sales) => {
    //     dispatch(setUserByL(sales.client))
    //   })
    Http.post('user/byl', { rt: rt })
      .then((res) => {
        // console.log(res)
        dispatch(setUserByL(res.data))
      })
  }
}

export const getSales = (rt) => {
  return (dispatch) => {
    dispatch(loading(true))
    // ByL.Programas(email)
    //   .then((sales) => {
    //     dispatch(setSales(sales))
    //   })
    Http.post('user/sales', { rt: rt })
      .then((res) => {
        // console.log(res.data)
        dispatch(setSales(res.data))
      })
  }
}