export const GET_USER = 'GET_USER';
export const REQUEST_USER = 'REQUEST_USER';
export const RECEIVE_USER = 'RECEIVE_USER';
export const SET_USER = 'SET_USER';
export const SET_USER_BYL = 'SET_USER_BYL';
export const SET_SALES = 'SET_SALES';
export const SET_LOAD = 'SET_LOAD';


// export { GET_USER };