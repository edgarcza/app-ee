
import axios from 'axios';

export default class ByL {

  email = null;
  estudiante = null;
  saleId = null;

  constructor(email = null) {
    this.email = email;
  }

  async Buscar() {
    let Cursos = await axios
      .get('https://server.bookandlearn.com/masterkey/agency/sale', {
        headers: {
          'X-Auth-Tenant': 'embajadores',
          'X-Auth-Token': 'B2FTChqGHobwQwmsdJ6uwxHBGzTewc55'
        },
        params: {
          status: 'Approved',
          q: this.email
        }
      });
    // console.log(Cursos.data);
    if (Cursos.data.resourceList.length < 1)
      return false;

    this.estudiante = Cursos.data.resourceList[0].student;
    this.saleId = Cursos.data.resourceList[0].id;
    return true;
  }

  static async Datos(correo) {
    let Cursos = await axios
      .get('https://server.bookandlearn.com/masterkey/agency/sale', {
        headers: {
          'X-Auth-Tenant': 'embajadores',
          'X-Auth-Token': 'B2FTChqGHobwQwmsdJ6uwxHBGzTewc55'
        },
        params: {
          status: 'Approved',
          q: correo
        }
      });
    // console.log(Cursos.data);
    return Cursos.data.resourceList[0];
  }

  static async Sale(saleId) {
    let Sale = await axios
      .get('https://server.bookandlearn.com/masterkey/agency/sale/' + saleId, {
        headers: {
          'X-Auth-Tenant': 'embajadores',
          'X-Auth-Token': 'B2FTChqGHobwQwmsdJ6uwxHBGzTewc55'
        }
      });
    return Sale.data;
  }

  static async Quote(saleId) {
    let Sale = await axios
      .get('https://server.bookandlearn.com/masterkey/agency/sale/' + saleId + '/quote', {
        headers: {
          'X-Auth-Tenant': 'embajadores',
          'X-Auth-Token': 'B2FTChqGHobwQwmsdJ6uwxHBGzTewc55'
        }
      });
    return Sale.data;
  }

  static async Programas(correo) {
    let Sale = await axios
      .get('https://server.bookandlearn.com/masterkey/agency/sale/', {
        headers: {
          'X-Auth-Tenant': 'embajadores',
          'X-Auth-Token': 'B2FTChqGHobwQwmsdJ6uwxHBGzTewc55'
        },
        params: {
          status: 'Approved',
          q: correo,
        }
      });
    let ProgramasPre = Sale.data.resourceList;
    // console.log(ProgramasPre);
    
    var Programas = await Promise.all(
      ProgramasPre.map(async (programa) => {
        // console.log(programa.id)
        var Prog = await ByL.Quote(programa.id);
        return Prog.resourceList[0];
      })
    )
    // console.log(Programas);
    return Programas;
  }
}