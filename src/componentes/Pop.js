import React, { Component } from 'react';
import { StyleSheet, Text, View, Animated, TouchableHighlight, KeyboardAvoidingView, Keyboard, BackHandler } from 'react-native';
import { BottomNavigation } from 'react-native-paper';
import { withTheme } from 'react-native-paper';

class Pop extends Component {
	state = {
		animFondo: new Animated.Value(0),
		animPop: null,
		// animPop: new Animated.Value(-(this.props.height + 10)),
	}
	teclado = false;

	constructor() {
		super();
	}

	componentWillMount() {
		this.setState({ animPop: new Animated.Value(-(this.props.height + 10)) })
		this.keyboardWillShowSub = Keyboard.addListener('keyboardDidShow', this.keyboardWillShow);
		this.keyboardWillHideSub = Keyboard.addListener('keyboardDidHide', this.keyboardWillHide);
		// this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.Unpop);
	}

	componentWillUnmount() {
			this.keyboardWillShowSub.remove();
			this.keyboardWillHideSub.remove();
			// this.backHandler.remove();
			this.Unpop();
			console.log('pop unmontado');
	}

	componentDidMount() {
		// console.log('pop montado');
		// this.fondoTo(1);
		this.Pop();

	}

	keyboardWillShow = (event) => {
		// console.log(event.endCoordinates);
		this.teclado = true;
		
		Animated.timing(this.state.animPop, {
			duration: 100,
			toValue: event.endCoordinates.height,
		}).start();
	};

	keyboardWillHide = (event) => {
		Animated.timing(this.state.animPop, {
			duration: 100,
			toValue: 0,
		}).start();
	};

	Pop = () => {
		Animated.timing(
			this.state.animFondo, {
			toValue: 1,
			duration: 200
		}
		).start((arg) => {
			if(this.teclado) return;
			Animated.timing(
				this.state.animPop, {
				toValue: -10,
				duration: 400
			}
			).start();
		});
	}

	Unpop = () => {
		Animated.timing(
			this.state.animPop, {
			toValue: -(this.props.height + 10),
			duration: 400
		}
		).start((arg) => {
			Animated.timing(
				this.state.animFondo, {
				toValue: 0,
				duration: 200
			}
			).start();
		});
		return true;
	}

	render() {
		const { animFondo, animPop } = this.state;
		return (
			<Animated.View style={[styles.container, { opacity: animFondo }]} >
				<TouchableHighlight style={styles.th} onPress={() => { this.props.onClose() }} underlayColor={'#00000000'}>
					<KeyboardAvoidingView behavior='padding' style={[styles.th, {}]}>
						<Animated.View style={[styles.pop, { bottom: animPop, height: this.props.height }]}>
							{this.props.children}
						</Animated.View>
					</KeyboardAvoidingView>
				</TouchableHighlight>
			</Animated.View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		zIndex: 100,
		flex: 1,
		backgroundColor: '#00000066',
		position: 'absolute',
		width: '100%',
		height: '100%',
	},
	th: {
		flex: 1,
	},
	pop: {
		// alignItems: 'center',
		// justifyContent: 'center',
		padding: 10,
		backgroundColor: '#fff',
		position: 'absolute',
		bottom: 0,
		elevation: 2,
		// borderRadius: 15,
		borderTopLeftRadius: 15,
		borderTopRightRadius: 15,
		width: '100%',
		zIndex: 101
		// height: 300,
	},
});

export default withTheme(Pop);