import React, { Component } from "react";
import {
	View,
	Text,
	StyleSheet,
	Image
} from "react-native";
import { withTheme } from 'react-native-paper';
import { withNavigation } from 'react-navigation';
import { Button, IconButton } from 'react-native-paper';
// import Icon from 'react-native-vector-icons/FontAwesome';

class HeaderEE extends Component {
	render() {
		return (
			<View style={[styles.container, {backgroundColor: this.props.theme.colors.background}]}>
				<IconButton
					icon='menu'
					size={40}
					color={this.props.theme.colors.primary}
					style={{ width: 40, position: 'absolute', top: 7, left: 4 }}
					onPress={() => this.props.navigation.toggleDrawer()}
				/>
				{/* <Text style={{...styles.ee, color: this.props.theme.colors.primary}}>Estudiantes Embajadores</Text> */}
				<Image
					style={{ width: '30%', height: 40, marginTop: 3 }}
					source={{ uri: 'https://i.ibb.co/Z228JLk/Logo-EE-Azul.png' }}
				/>
				<Text></Text>
			</View>
		);
	}
}
export default withNavigation(withTheme(HeaderEE));

const styles = StyleSheet.create({
	container: {
		textAlign: 'left',
		paddingTop: 25,
		flexDirection: 'row',
		justifyContent: 'center'
	},
	ee: {
		// fontWeight: '',
		marginTop: 7,
		fontSize: 25,
	}
});