import React, { Component } from 'react';
import { StyleSheet, Image, View } from 'react-native';
import { withTheme, ProgressBar } from 'react-native-paper';
import Firebase from '../Firebase';
import 'firebase/auth';

class Cargando extends Component {
	constructor() {
		super();
	}

	componentDidMount() {
		const unsubscribe = Firebase.auth().onAuthStateChanged(user => {
			unsubscribe();
			if (user) this.setState({ sesion: true })
			// resolve();
		});
	}

	render() {
		return (
			<View style={styles.container}>
				{/* <Text>Cargando</Text> */}
				<ProgressBar progress={0.5} indeterminate={true} style={{width: '70%'}} />
				{/* <Image style={{ width: '100%', height: '100%', resizeMode: 'cover' }} source={require('../../assets/splash.png')} /> */}
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
	},
});

export default withTheme(Cargando);