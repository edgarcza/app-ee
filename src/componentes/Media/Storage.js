import * as ImageManipulator from 'expo-image-manipulator';
import Firebase from '../../Firebase';
import 'firebase/storage';
import uuidv4 from 'uuid/v4'

export default class Storage {
  Carpeta = null;

  constructor(carpeta) {
    this.Carpeta = carpeta;
  }

  async Subir2(media) {
      var euuid = uuidv4();
      var FComp = await ImageManipulator.manipulateAsync(media.uri, [], { compress: 0.1 });
      var FFetch = await fetch(FComp.uri);
      var FBlob = await FFetch.blob();

      var SubRef = Firebase.storage().ref(`${this.Carpeta}/${euuid}.jpg`);
      var Subida = await SubRef.put(FBlob);
      var Url = await SubRef.getDownloadURL();

      return { id: euuid, url: Url }
  }

  static async Subir(media) {
      var euuid = uuidv4();
      var FComp = await ImageManipulator.manipulateAsync(media.uri, [], { compress: 0.2, base64: true });

      // return { id: euuid, url: Url }
      return { id: euuid, uri: FComp.uri, base64: FComp.base64 }
  }

  async SubirVarios(media) {
    mediaArray = await Promise.all(media.map(async (media, i) => {
      var euuid = uuidv4();
      // console.log(media.uri + ' |||| ' + euuid);
      var FComp = await ImageManipulator.manipulateAsync(media.uri, [], { compress: 0.1 });
      var FFetch = await fetch(FComp.uri);
      var FBlob = await FFetch.blob();

      var SubRef = Firebase.storage().ref(`${this.Carpeta}/${euuid}.jpg`);
      var Subida = await SubRef.put(FBlob);
      var Url = await SubRef.getDownloadURL();

      return { id: euuid, url: Url }
    }));

    return mediaArray;
  }
}