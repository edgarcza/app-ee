import React, { Component } from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import { BottomNavigation, withTheme } from 'react-native-paper';
import { Header } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import Noticias from './Noticias';

class Principal extends Component {

	constructor() {
		super();
	}

	render() {
		return (
			<View>
				<View>
					<Header
						leftComponent={{ icon: 'menu', color: '#fff' }}
						centerComponent={{ text: 'MY TITLE', style: { color: '#fff' } }}
						rightComponent={{ icon: 'home', color: '#fff' }}
					/>
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
	},

	side: {
		position: 'absolute',
		flex: 1,
		backgroundColor: '#111',
		width: 50,
		height: 50
	}
});

export default withTheme(Principal);