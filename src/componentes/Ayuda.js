import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { BottomNavigation } from 'react-native-paper';
import { withTheme } from 'react-native-paper';
import HeaderEE from './Header';

class Ayuda extends Component {
	constructor() {
		super();
	}

	render() {
		return (
			<View style={styles.container}>
				<HeaderEE></HeaderEE>
				<Text>Ayuda</Text>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		// alignItems: 'center',
		// justifyContent: 'center',
	},
});

export default withTheme(Ayuda);