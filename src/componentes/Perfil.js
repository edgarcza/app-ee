import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableHighlight, AsyncStorage, FlatList } from 'react-native';
import {
	Avatar,

} from 'react-native-paper';
import { withTheme, ActivityIndicator } from 'react-native-paper';
import { connect } from 'react-redux'
import { getUserByL, getUser, saveUser } from '../store/Actions'
import Storage from './Media/Storage'
import Firebase from '../Firebase';
import { Http } from '../Config';
import Publicacion from './Inicio/Mundo/Publicacion';

class Perfil extends Component {
	state = {
		usuario: {},
		avatarLoading: false,
		publicaciones: [],
		postsLoading: false,
	}

	UID = null;

	constructor() {
		super();
	}

	componentDidMount() {
		this.CargarUsuario();
		this.CargarPublicaciones();
	}

	CargarUsuario = async () => {
		this.RT = await AsyncStorage.getItem('rt');
		// const UsReq = await Firebase.firestore()
		// 	.collection('users')
		// 	.doc(this.UID)
		// 	.get();
		// this.setState({usuario: UsReq.data()})
		this.props.getUser(this.RT);
	}

	CambiarFoto = () => {
		this.props.navigation.navigate('Media', { cb: this.CambioFoto.bind(this) });
	}

	CambioFoto = async (a) => {
		// console.log(a[0])
		// var St = new Storage('imagenes/perfil');
		// var Subida = await St.Subir(a[0]);
		this.setState({ avatarLoading: true });
		var Subir = await Storage.Subir(a[0]);
		// console.log(Subir.uri);
		var Res = await Http.post('user/avatar', { avatar: Subir, rt: this.RT });
		console.log(Res);
		if (!Res.error) {
			this.props.getUser(this.RT);
			this.setState({ avatarLoading: false });
		}
	}

	CargarPublicaciones = async () => {
		// console.log('CargarPublicaciones');
		this.setState({ postsLoading: true });
		// let pubs = this.state.publicaciones;

		try {
			var rt = await AsyncStorage.getItem('rt');
			var Req = await Http.post('posts/profile', { rt: rt, profile: rt });
			// var Req = await Http.post('posts', { rt: rt });
			// console.log(Req.data);
			this.setState({ publicaciones: Req.data, postsLoading: false });

		} catch (error) {
			console.log(error);
		}

	}

	render() {
		const { user } = this.props;
		const { publicaciones, postsLoading } = this.state;
		return (
			<View style={styles.container}>
				<View style={[styles.c_avatar]}>
					<TouchableHighlight onPress={() => this.CambiarFoto()}>
						<View>
							<Avatar.Image style={styles.avatar} size={120} source={{ uri: user.avatar }} />
							{this.state.avatarLoading &&
								<View style={[styles.carga]}>
									<ActivityIndicator animating={true} size={60} />
								</View>
							}
						</View>
					</TouchableHighlight>
				</View>
				<View style={[styles.c_datos]}>
					<Text style={{ fontSize: 24, fontWeight: 'bold', marginBottom: 10 }}>{user.name}</Text>
					<FlatList
						data={publicaciones}
						extraData={this.state}
						renderItem={({ item }) => <Publicacion datos={item} comentar={this.AbrirComentar} profile={true} />}
						keyExtractor={(publicacion, i) => publicacion.main_id.toString()}
						onEndReachedThreshold={0.1}
						refreshing={postsLoading}
						onRefresh={() => { this.CargarPublicaciones() }}
					// onEndReached={() => { this.CargarPublicaciones() }}
					/>
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},

	c_avatar: {
		backgroundColor: '#ededed',
		paddingVertical: 10,
		justifyContent: 'center',
		alignItems: 'center'
	},

	carga: {
		position: 'absolute',
		zIndex: 100,
		// left: 30,
		// top: 30,
		width: 120,
		height: 120,
		borderRadius: 120,
		padding: 30,
		backgroundColor: '#eeeeee55'
		// justifyContent: 'center',
		// alignContent: 'center',
		// alignItems: 'center'
	},

	c_datos: {
		flex: 1,
		padding: 5,
		justifyContent: 'center',
		alignContent: 'center',
		alignItems: 'center'
	},

});


const mapStateToProps = (state) => {
	return {
		user: state.user,
		loading: state.loading
	};
}
const mapDispatchToProps = (dispatch) => {
	return {
		getUser: (uid) => dispatch(getUser(uid)),
		// saveUser: (uid, data) => dispatch(saveUser(uid, data)),
		// getUserByL: (email) => dispatch(getUserByL(email))
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(Perfil));

// export default withTheme(Perfil);