import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { BottomNavigation } from 'react-native-paper';
import { withTheme } from 'react-native-paper';

class Base extends Component {
	constructor() {
		super();
	}

	render() {
		return (
			<View style={styles.container}>
				<Text>Base</Text>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
	},
});

export default withTheme(Base);