import React, { Component } from 'react';
import { StyleSheet, Text, View, AsyncStorage, Picker, TouchableHighlight } from 'react-native';
import { withNavigation } from 'react-navigation';
// import { BottomNavigation } from 'react-native-paper';
import { withTheme } from 'react-native-paper';
import { Http } from '../../Config';

const SelectComp = (props) => {
  const { colors } = props.theme;
  return (
    <View style={{ borderWidth: 1, borderColor: colors.primary, borderRadius: 10 }}>
      <Text style={{ position: 'absolute', top: -12, left: 15, fontWeight: 'bold', color: colors.primary, backgroundColor: colors.background }}>
        {props.label}
      </Text>
      <Picker
        selectedValue={props.value}
        onValueChange={(itemValue, itemIndex) =>
          props.onValueChange(itemValue, itemIndex)
        }>
        {props.options.map((option, i) => (
          <Picker.Item key={i} label={option.label} value={option.value} />
        ))}
      </Picker>
    </View>
  )
}
export const Select = withTheme(SelectComp);

const PaypalMet = (props) => {
  return (
    <TouchableHighlight style={[styles.pmPaypalBox]} onPress={() => { props.onPress() }} underlayColor='#eee'>
      <Text style={[styles.pmPaypal]}>{props.label}</Text>
    </TouchableHighlight>
  )
}
export const PaypalMethod = withTheme(PaypalMet);

const CardMet = (props) => {
  return (
    <TouchableHighlight style={[styles.pmCardBox]} onPress={() => { props.onPress() }} underlayColor='#eee'>
      <Text style={[styles.pmCard]}>{props.label}</Text>
    </TouchableHighlight>
  )
}
export const CardMethod = withTheme(CardMet);

class Pago extends Component {

  state = {
    total: 0,
    payMethod: 1,
    payMethods: [
      // { type: 1, value: 'x-4343' },
      // { type: 2, value: '' }
    ]
  }

  pmOptions = [
    { value: 1, label: 'Tarjeta de crédito/débito' },
    { value: 2, label: 'Paypal' }
  ]

  constructor() {
    super();
  }

  componentDidMount() {
    this.loadCart();
  }

  loadCart = async () => {
    this.RT = await AsyncStorage.getItem('rt');
    let Res = await Http.post('shop/cart-items', { rt: this.RT });
    if (!Res.error) {
      this.setItems(Res.data);
    }
    this.loadMethods();
  }

  loadMethods = async () => {
    this.RT = await AsyncStorage.getItem('rt');
    try {
      let Res = await Http.post('paymethods/get', { rt: this.RT })
      // console.log(Res)
      if (!Res.error) {
        this.setState({ payMethods: Res.data });
      }
    } catch (error) {

    }
  }

  setItems = (items) => {
    var total = 0;
    items.forEach((item) => { total += parseFloat(item.sh_price) });
    this.setState({ items, total })
  }

  selectedMethod = (pm) => {
    console.log(pm)
  }

  render() {
    const { total, items, payMethod, payMethods } = this.state;
    const { colors } = this.props.theme;
    return (
      <View style={[styles.container, { backgroundColor: colors.background }]}>
        <View style={{ flexDirection: 'row', justifyContent: 'flex-end', paddingVertical: 10, paddingHorizontal: 5 }}>
          <Text style={{ fontSize: 20, color: colors.primary }}>Total de ${total}</Text>
        </View>
        {/* <View style={{ paddingHorizontal: 15, paddingVertical: 10 }}>
          <Text style={{ textAlign: 'center', fontSize: 20 }}>Seleccionar método de pago</Text>
          <Select value={payMethod} label={'Método de pago'} onValueChange={(itemVal) => { this.setState({ payMethod: itemVal }) }} options={this.pmOptions} />
        </View> */}
        <View style={{ borderWidth: 2, borderColor: colors.primary, marginVertical: 20, marginHorizontal: 20, borderRadius: 5 }}>
          <Text style={{ textAlign: 'center', fontSize: 20, position: 'absolute', top: -16, backgroundColor: colors.background, left: 15, paddingHorizontal: 3, color: colors.primary }}>Seleccionar método de pago</Text>
        </View>
        <View style={{ paddingHorizontal: 15, paddingVertical: 10 }}>
          {payMethods.length < 1 &&
            <TouchableHighlight style={[styles.pmNoneBox, { backgroundColor: colors.background }]} onPress={() => { this.props.navigation.navigate('Pagos'); }} underlayColor='#eee'>
              <View>
                <Text style={{ textAlign: 'center' }}>No hay métodos de pago configurados</Text>
                <Text style={{ textAlign: 'center' }}>Ir a <Text style={{ textDecorationLine: 'underline' }}>Configuración de Pagos</Text></Text>
              </View>
            </TouchableHighlight>
          }

          {payMethods.map((pm, i) => {
            if (pm.type === 1)
              return (
                <CardMethod key={i} label={pm.number} onPress={() => { this.selectedMethod(pm) }} />
              )
            else if (pm.type === 2)
              return <PaypalMethod key={i} label={pm.paypal} onPress={() => { }} />
          })}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  pmPaypalBox: {
    backgroundColor: '#169BD7',
    marginVertical: 10,
    paddingVertical: 5,
    elevation: 2,
    borderRadius: 5
  },
  pmPaypal: {
    color: '#fff',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 22,
  },

  pmCardBox: {
    backgroundColor: '#A9B0B5',
    marginVertical: 10,
    paddingVertical: 5,
    elevation: 2,
    borderRadius: 5
  },
  pmCard: {
    color: '#fff',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 22,
  },

  pmNoneBox: {
    borderWidth: 1,
    borderColor: '#000',
    marginVertical: 10,
    paddingVertical: 5,
    elevation: 2,
    borderRadius: 5,
  }
});

export default withNavigation(withTheme(Pago));