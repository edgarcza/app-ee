import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image, Picker, BackHandler, Animated, AsyncStorage } from 'react-native';
import { IconButton, Button, ToggleButton } from 'react-native-paper';
import { withTheme } from 'react-native-paper';
import Loading from '../Loading'
import HeaderEE from '../Header';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { Http } from '../../Config';


const CategoriaComp = (props) => (
	<View>
		<View style={[styles.tiendaCategoria, { backgroundColor: props.theme.colors.background }]}>
			<View>
				<Text style={[styles.tcTitulo, { color: props.theme.colors.text }]}>{props.title}</Text>
			</View>
			<ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={[styles.tcScroll]}>
				{props.children}
			</ScrollView>
		</View>
	</View>
)
const TiendaCategoria = withTheme(CategoriaComp);

const CaracsComp = (props) => {

	return (
		<View style={{ paddingVertical: 5 }}>
			<Text style={[{ fontSize: 17, fontWeight: "bold", color: props.theme.colors.primary }]}>{props.title}</Text>
			<View style={{ paddingVertical: 4 }}>
				{props.children}
			</View>
		</View>
	)
}
const TiendaDetalles = withTheme(CaracsComp);

const ItemComp = (props) => (
	<View style={[styles.tiendaItem]}>
		<Image source={{ uri: props.img }} style={[styles.tiImage]} />
		<View style={[styles.tiAcciones]}>
			<View>
				<Text style={[styles.tiPrice]}>${props.price}</Text>
			</View>
			<View style={{ paddingTop: 3 }}>
				<IconButton
					icon={({ size, color }) => (<Icon name="cart-plus" color={color} size={size} />)}
					color={'#fff'}
					// style={{ width: 50, height: 50 }}
					size={20}
					onPress={() => { props.add(props.id, props.price, props.img) }}
				/>
			</View>
		</View>
	</View>
)
const TiendaItem = withTheme(ItemComp);

class Tienda extends Component {
	constructor() {
		super();
	}

	state = {
		items: [],
		itemInfo: { size_id: null, color_id: null },
		shop: [],
		addModal: null,
		addModalDetails: [],
		addModalAnim: new Animated.Value(0),
		itemSize: [],
		itemColor: [],
		addModalLoading: false,
	}

	componentDidMount() {
		this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackHandler);
		this.loadShop();
	}

	componentWillUnmount() {
		this.backHandler.remove()
	}

	handleBackHandler = () => {
		if (this.state.addModal) {
			this.setState({ addModal: null, addModalAnim: new Animated.Value(0) })
			return true;
		}
		return false;
	}

	loadShop = async () => {
		this.RT = await AsyncStorage.getItem('rt');
		let Res = await Http.post('shop/load', { rt: this.RT });
		if (!Res.error)
			this.setState({ shop: Res.data })
		console.log(Res);
		this.loadCartItems();
	}

	itemDetails = async (id, price, img) => {
		let Res = await Http.post('shop/item-details', { rt: this.RT, id });
		if (!Res.error) {
			// this.itemDetailsData = Res.data;
			if (Res.data.length < 1) return;
			let colors = Res.data[0].colors.map((color) => color)
			this.setState({
				addModal: id,
				itemSize: Res.data,
				itemColor: colors,
				itemInfo: {
					size_id: Res.data[0].id,
					color_id: Res.data[0].colors[0].main_id,
				}
			})
			// this.setState({ addModalDetails: Res.data, addModal: id })
			Animated.timing(
				this.state.addModalAnim, {
				toValue: 1,
				duration: 600,
			}
			).start()
		}
	}

	itemDetailsChange = (value, i) => {
		let colors = this.state.itemSize[i].colors.map((color) => color)
		this.setState({ itemInfo: { size_id: value }, itemColor: colors })
	}

	addToCart = async () => {
		// console.log(this.state.itemInfo)
		this.setState({ addModalLoading: true });
		let Res = await Http.post('shop/add-to-cart', { rt: this.RT, id: this.state.itemInfo.color_id });
		if (!Res.error) {
			let items = this.state.items;
			items.push({ id: Res.data.id });
			this.setState({ addModalLoading: false, items, addModal: null, addModalAnim: new Animated.Value(0), addModalLoading: false });
		}
	}

	loadCartItems = async () => {
		let Res = await Http.post('shop/cart-items', { rt: this.RT });
		if (!Res.error)
			this.setState({ items: Res.data });
	}

	goToPay = () => {
		this.props.navigation.navigate('Orden');
	}

	render() {
		const { colors } = this.props.theme;
		const { items, itemInfo, addModal, addModalAnim, addModalLoading, shop, itemSize, itemColor } = this.state;
		return (
			<View style={styles.container}>
				{/* <HeaderEE></HeaderEE> */}
				<Text style={[{ color: this.props.theme.colors.primary }, styles.title]}>Tienda</Text>

				<ScrollView>
					{shop.map((category, ci) => (
						<TiendaCategoria key={ci} title={category.shop_category}>
							{category.items.map((item, ii) => (
								<TiendaItem key={ii} price={item.sh_price} id={item.id} img={item.sh_image} add={this.itemDetails} />
							))}
						</TiendaCategoria>
					))}
				</ScrollView>

				{addModal &&
					<Animated.View style={[styles.tiendaModalBox, { opacity: addModalAnim }]}>
						<Animated.View style={[styles.tiendaModal]}>
							<TiendaDetalles title={'Talla'}>
								<View style={[styles.tiendaPicker, { borderColor: colors.primary }]}>
									<Picker
										selectedValue={this.state.itemInfo.size_id}
										onValueChange={(itemValue, itemIndex) => {
											this.itemDetailsChange(itemValue, itemIndex)
										}
										}>
										{itemSize.map((details, id) => (
											<Picker.Item key={id} label={details.shd_size} value={details.id} />
										))}
									</Picker>
								</View>
							</TiendaDetalles>
							<TiendaDetalles title={'Color'}>
								<View style={[styles.tiendaPicker, { borderColor: colors.primary }]}>
									<Picker
										selectedValue={this.state.itemInfo.color_id}
										onValueChange={(itemValue, itemIndex) =>
											this.setState({ itemInfo: { ...this.state.itemInfo, color_id: itemValue } })
										}>
										{itemColor.map((color, ic) => (
											<Picker.Item key={ic} label={color.shd_color} value={color.main_id} />
										))}
									</Picker>
								</View>
							</TiendaDetalles>

							<Button disabled={addModalLoading} loading={addModalLoading} icon="cart-plus" mode="contained" onPress={() => { this.addToCart() }}>
								Agregar
							</Button>
						</Animated.View>
					</Animated.View>
				}


				{items.length > 0 &&
					<View style={[styles.tiendaCarritoBox]}>
						<View style={[styles.tiendaCarrito, { backgroundColor: colors.primary + 'dd' }]}>
							<View>
								<Text style={[{ color: colors.background, fontSize: 18 }]}>
									{items.length} producto(s)
								</Text>
							</View>
							<View>
								<Button mode="contained" color={colors.background} onPress={this.goToPay}>
									Pagar
  							</Button>
							</View>
						</View>
					</View>
				}


			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		// alignItems: 'center',
		// justifyContent: 'center',
	},

	title: {
		fontSize: 24,
		marginLeft: 14,
		marginBottom: 5,
		fontWeight: 'bold'
	},

	tiendaCategoria: {
		elevation: 1,
		padding: 5,
		paddingBottom: 10,
		marginHorizontal: 0,
		marginVertical: 14,
		// marginBottom: 7,
		// marginTop: 1,
	},

	tiendaItem: {
		backgroundColor: '#fff',
		height: 200,
		width: 200,
		marginRight: 10,
		marginLeft: 4,
		marginTop: 3,
		elevation: 2,
		padding: 8
	},

	tiImage: {
		width: 170,
		height: 170,
		resizeMode: 'contain'
	},

	tiAcciones: {
		flexDirection: 'row',
		position: 'absolute',
		bottom: 0,
		right: 0,
		backgroundColor: '#3d72b8AA',
		borderTopLeftRadius: 10,
	},

	tiPrice: {
		paddingVertical: 10,
		paddingLeft: 9,
		fontSize: 20,
		fontWeight: 'bold',
		color: '#fff'
	},

	tcTitulo: {
		marginHorizontal: 10,
		fontSize: 20,
		fontWeight: 'bold',
		// textTransform: 'uppercase'
	},

	tcScroll: {
		paddingVertical: 3,
	},

	tiendaCarritoBox: {
		position: 'absolute',
		bottom: 10,
		justifyContent: 'center',
		alignItems: 'center',
		width: '100%',
		height: 100,
	},

	tiendaCarrito: {
		backgroundColor: '#ffffff00',
		width: '80%',
		height: 70,
		// elevation: 2,
		borderRadius: 10,
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		paddingHorizontal: 20
	},

	tiendaModalBox: {
		position: "absolute",
		height: '100%',
		width: '100%',
		backgroundColor: '#00000077',
		justifyContent: 'center',
		alignItems: 'center',
	},

	tiendaModal: {
		backgroundColor: '#fff',
		width: '80%',
		minHeight: 160,
		elevation: 2,
		borderRadius: 15,
		padding: 13,
	},

	tiendaPicker: {
		// width: '100%',
		// color: '#fff'
		borderWidth: 1,
		borderRadius: 5,
		marginTop: 4,
	},
});

export default withTheme(Tienda);