import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableHighlight, ScrollView } from 'react-native';
// import { BottomNavigation } from 'react-native-paper';
import { withNavigation } from 'react-navigation';
import { withTheme, Button } from 'react-native-paper';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { Http } from '../../Config';
import { AsyncStorage } from 'react-native';

const COComp = (props) => (
  <View style={{ backgroundColor: props.theme.colors.background, elevation: 1, marginVertical: 10 }}>
    <View style={{ flexDirection: 'row', paddingHorizontal: 5 }}>
      <View>
        <Image source={{ uri: props.img }} style={{ width: 135, height: 135, resizeMode: 'contain' }} />
      </View>
      <View style={{ justifyContent: 'center', paddingLeft: 10 }}>
        <Text style={[styles.itemDesc]}>— {props.name}</Text>
        <Text style={[styles.itemDesc]}>— {props.size}</Text>
        <Text style={[styles.itemDesc]}>— {props.color}</Text>
        <Text style={[styles.itemPrice, { color: props.theme.colors.primary }]}>${props.price}</Text>
      </View>
      <TouchableHighlight style={{ position: 'absolute', bottom: 5, right: 5, padding: 3 }} underlayColor='#eee' onPress={() => { props.onDelete(props.id) }}>
        <Text style={{ fontSize: 14, fontWeight: 'bold', color: '#c92724' }}>ELIMINAR</Text>
      </TouchableHighlight>
    </View>
  </View>
)
const OrdenItem = withTheme(COComp);

class Orden extends Component {

  state = {
    items: [],
    total: 0
  }

  constructor() {
    super();
  }

  componentDidMount() {
    this.loadCart();
  }

  loadCart = async () => {
    this.RT = await AsyncStorage.getItem('rt');
    let Res = await Http.post('shop/cart-items', { rt: this.RT });
    if (!Res.error) {
      this.setItems(Res.data);
    }
  }

  deleteItem = async (id) => {
    console.log(id)
    let Res = await Http.post('shop/cart-item/delete', { rt: this.RT, id });
    if (!Res.error)
      this.setItems(Res.data);
  }

  setItems = (items) => {
    var total = 0;
    items.forEach((item) => { total += parseFloat(item.sh_price) });
    this.setState({ items, total })
  }

  pay = () => {
    this.props.navigation.navigate('Pago');
  }

  render() {
    const { items, total } = this.state;
    return (
      <View style={styles.container}>
        <ScrollView>
          {items.map((item, i) => (
            <OrdenItem
              key={i}
              img={item.sh_image}
              name={item.sh_item}
              price={item.sh_price}
              color={item.shd_color}
              size={item.shd_size}
              id={item.main_id}
              onDelete={this.deleteItem}
            />
          ))}
        </ScrollView>
        <View style={{ backgroundColor: this.props.theme.colors.primary }}>
          <View style={{ flexDirection: 'row', justifyContent: 'flex-end', padding: 10 }}>
            <Text style={{ fontSize: 32, fontWeight: 'bold', color: '#fff' }}>Total de ${total}</Text>
          </View>

          <View style={{ flexDirection: 'row', justifyContent: 'flex-end', padding: 10 }}>
            <Button mode='contained' onPress={this.pay} labelStyle={{ fontSize: 24, flexDirection: 'row' }} color="#fff">
              <Text>pagar</Text>
            </Button>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // alignItems: 'center',
    // justifyContent: 'center',
  },
  itemDesc: {
    fontSize: 18,
    marginVertical: 1,
    marginHorizontal: 7,
  },
  itemPrice: {
    fontSize: 28,
    fontWeight: 'bold',
    margin: 5,
    // marginHorizontal: 10,
    // marginVertical: 10,
  }
});

export default withNavigation(withTheme(Orden));