import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableHighlight, AsyncStorage } from 'react-native';
import { Switch } from 'react-native-paper';
import { withTheme } from 'react-native-paper';
import { ConfigTitulo } from './Configuracion'
import { Http } from '../../Config'
import Loading from '../Loading'

const CSwitchOption = (props) => {
	const { colors } = props.theme;
	// console.log(colors);

	return (
		<View>
			<TouchableHighlight onPress={() => { }} underlayColor={'#eee'} style={{ paddingVertical: 10, paddingHorizontal: 5, marginHorizontal: 8, borderBottomColor: '#eee', borderBottomWidth: 1 }} >
				<View style={{ flexDirection: 'row' }}>
					<Text style={[styles.text, { color: colors.text }]}>{props.title}</Text>
					<Switch
						color={colors.primary}
						// color={'#000'}
						style={{ position: 'absolute', right: 10 }}
						value={props.value}
						onValueChange={() => { props.onSwitch() }
						}
					/>
				</View>
			</TouchableHighlight>
		</View>
	)
}
export const ConfigSwitch = withTheme(CSwitchOption);

class NotificacionesConfig extends Component {

	state = {
		nc_weather: 0,
		nc_city: 0,
		nc_message: 0,
		nc_comment: 0,
		nc_like: 0,
		loading: true,
	}

	constructor() {
		super();
	}

	componentDidMount() {
		this.CargarNots();
	}

	CargarNots = async () => {
		this.setState({loading: true})
		this.RT = await AsyncStorage.getItem('rt');
		var Res = await Http.post('user/nots', { rt: this.RT });
		// console.log(Res);
		if (!Res.error) {
			this.setState({
				nc_weather: Res.data.nc_weather,
				nc_city: Res.data.nc_city,
				nc_message: Res.data.nc_message,
				nc_comment: Res.data.nc_comment,
				nc_like: Res.data.nc_like,
				loading: false
			})
		}

	}

	Switch = async () => {
		// this.setState
		// console.log(this.state);

		var Res = await Http.post('user/nots/save', { rt: this.RT, nots: this.state });
		// console.log(Res);
		if (!Res.error) {
			this.setState({
				nc_weather: Res.data.nc_weather,
				nc_city: Res.data.nc_city,
				nc_message: Res.data.nc_message,
				nc_comment: Res.data.nc_comment,
				nc_like: Res.data.nc_like,
			})
		}
	}

	render() {
		const { nc_weather, nc_city, nc_message, nc_comment, nc_like, loading } = this.state;
		const { colors } = this.props.theme;
		return (
			<View style={styles.container}>
				<Loading loading={loading} />

				<ConfigTitulo titulo='Viaje' />
				<ConfigSwitch title='Clima' value={nc_weather > 0} onSwitch={() => { this.setState({ nc_weather: nc_weather > 0 ? 0 : 1 }, () => { this.Switch() }); }} />
				<ConfigSwitch title='Datos de ciudad' value={nc_city > 0} onSwitch={() => { this.setState({ nc_city: nc_city > 0 ? 0 : 1 }, () => { this.Switch() }); }} />

				<ConfigTitulo titulo='Mundo embajador' />
				<ConfigSwitch title='Comentarios' value={nc_comment > 0} onSwitch={() => { this.setState({ nc_comment: nc_comment > 0 ? 0 : 1 }, () => { this.Switch() }); }} />
				<ConfigSwitch title='Likes' value={nc_like > 0} onSwitch={() => { this.setState({ nc_like: nc_like > 0 ? 0 : 1 }, () => { this.Switch() }); }} />

				<ConfigTitulo titulo='Otros' />
				<ConfigSwitch title='Mensajes' value={nc_message > 0} onSwitch={() => { this.setState({ nc_message: nc_message > 0 ? 0 : 1 }, () => { this.Switch() }); }} />
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		// alignItems: 'center',
		// justifyContent: 'center',
	},

	text: {
		fontSize: 16
	}
});

export default withTheme(NotificacionesConfig);