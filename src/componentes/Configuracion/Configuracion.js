import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableHighlight } from 'react-native';
import { withTheme } from 'react-native-paper';
import { withNavigation } from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome5';

const CTitulo = (props) => {
	const { colors } = props.theme;
	return (
		<View style={configStyles.ctitulo_bloque}>
			<Text style={[configStyles.ctitulo_titulo, { color: colors.primary }]}>{props.titulo}</Text>
		</View>
	)
}
export const ConfigTitulo = withTheme(CTitulo);

const CSeparador = (props) => {
	return (
		<View style={{ height: 10, width: '100%', backgroundColor: "#eee", borderTopColor: '#dbdbdb', borderTopWidth: 1 }}></View>
	)
}
export const ConfigSeparador = withTheme(CSeparador);

const COpcion = (props) => {
	const { colors } = props.theme;
	// console.log(props)
	return (
		<TouchableHighlight style={configStyles.copcion_bloque} underlayColor="#eee" onPress={() => { props.onPress() }}>
			<View>
				<Text style={[configStyles.copcion_mostrado, { color: colors.text }]}>{props.children}</Text>
				<Text style={[configStyles.copcion_nombre]}>{props.nombre}</Text>
			</View>
		</TouchableHighlight>
	);
}
export const ConfigOpcion = withTheme(COpcion);

class Configuracion extends Component {
	constructor() {
		super();
	}

	AConfig(Config) {
		this.props.navigation.navigate(Config)
	}


	Config = (props) => {
		return (
			<View>
				<TouchableHighlight onPress={() => { this.AConfig(props.route) }} style={styles.config_v} underlayColor='#ededed'>
					<View>
						<View style={{ width: 30, justifyContent: 'center', alignItems: 'center' }}>
							<Icon name={props.icon} size={24} color={'#ababab'} />
						</View>
						<View style={{ borderBottomWidth: 1, borderBottomColor: '#eee', width: '100%', paddingBottom: 15, position: 'absolute', left: 45 }}>
							<Text style={styles.config_t}>{props.config}</Text>
						</View>
					</View>
				</TouchableHighlight>
			</View>
		)
	}

	render() {
		const { colors } = this.props.theme;
		return (
			<View style={styles.container}>
				<ConfigTitulo titulo='Ajustes' />
				<this.Config config={'Datos'} route={'Datos'} icon={'clipboard'} />
				<this.Config config={'Pagos'} route={'Pagos'} icon={'money-bill-alt'} />
				<this.Config config={'Notificaciones'} route={'NotificacionesConfig'} icon={'bell'} />

				<ConfigSeparador />

				<ConfigTitulo titulo='Otros' />
				<this.Config config={'Bóveda'} route={'Boveda'} icon={'file'} />
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},

	config_v: {
		paddingTop: 10,
		paddingBottom: 15,
		paddingHorizontal: 20,
	},
	config_t: {
		fontSize: 18,
	}
});

const configStyles = StyleSheet.create({
	ctitulo_bloque: {
		paddingHorizontal: 10,
		paddingTop: 10,
		paddingBottom: 6,
	},
	ctitulo_titulo: {
		fontWeight: "bold",
		fontSize: 15,
		color: '#fff'
	},

  copcion_bloque: {
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderBottomColor: '#eee',
    borderBottomWidth: 1
  },
  copcion_mostrado: {
    fontSize: 15
  },
  copcion_nombre: {
    fontSize: 13,
    color: '#8c8c8c',
    marginTop: 2
  }


});

export default withNavigation(withTheme(Configuracion));