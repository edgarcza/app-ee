import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableHighlight, Alert, AsyncStorage } from 'react-native';
import { withTheme, TextInput, Button } from 'react-native-paper';
import { Select, PaypalMethod, CardMethod } from '../Tienda/Pago';
import { Http } from '../../Config';

class Pagos extends Component {

  state = {
    payMethod: 1,
    card: {
      owner: 'Edgar Cantú',
      exp_month: '1',
      exp_year: '2021',
      number: '4000400040004000',
    },
    payMethods: [],
    loading: false,
  }

  pmOptions = [
    { value: 1, label: 'Tarjeta de crédito/débito' },
    { value: 2, label: 'Paypal' }
  ]

  constructor() {
    super();
  }

  componentDidMount() {
    this.loadMethods();
  }

  addCard = async () => {
    const { payMethod, card } = this.state;
    this.setState({ loading: true })
    if (payMethod === 1) {
      // console.log('add card', card)
      if (card.number.length < 1 || card.exp_month.length < 1 || card.exp_year.length < 1 || card.owner.length < 1)
        return Alert.alert('Error', 'Llenar los campos')
      try {
        // console.log({ type: 1, pm: card, rt: this.RT })
        let Res = await Http.post('paymethod/add', { type: 1, pm: card, rt: this.RT });
        // console.log(Res)
        if (!Res.error) {
          this.setState({ loading: false, card: { number: '', owner: '', exp_month: '', exp_year: '' } })
          this.loadMethods();
        }
      } catch (error) {
        console.log(error)
      }
    }
  }

  loadMethods = async () => {
    this.RT = await AsyncStorage.getItem('rt');
    try {
      let Res = await Http.post('paymethods/get', { rt: this.RT })
      // console.log(Res)
      if (!Res.error) {
        this.setState({ payMethods: Res.data });
      }
    } catch (error) {

    }
  }

  deleteMethod = async (pm) => {
    try {
      let Res = await Http.post('paymethod/delete', { rt: this.RT, id: pm.id })
      // console.log(Res)
      if (!Res.error) {
        this.loadMethods();
      }
    } catch (error) {

    }
  }

  render() {
    const { payMethod, card, loading, payMethods } = this.state;
    return (
      <View style={styles.container}>
        <View>
          <Select value={payMethod} label={'Agregar método de pago'} onValueChange={(itemVal) => { this.setState({ payMethod: itemVal }) }} options={this.pmOptions} />
        </View>
        <View style={{ paddingVertical: 15 }}>
          {payMethod === 1 && (
            <View>
              <View>
                <TextInput
                  style={{ backgroundColor: '#fff', width: '100%' }}
                  label='Número de tarjeta'
                  value={card.number}
                  textContentType='creditCardNumber'
                  keyboardType='number-pad'
                  mode='outlined'
                  onChangeText={text => { this.setState({ card: { ...card, number: text } }); }}
                />
              </View>
              <View style={{ flexDirection: 'row' }}>
                <TextInput
                  style={{ backgroundColor: '#fff', width: '60%' }}
                  label='Titular'
                  mode='outlined'
                  value={card.owner}
                  onChangeText={text => { this.setState({ card: { ...card, owner: text } }); }}
                />
                <TextInput
                  style={{ backgroundColor: '#fff', width: '20%' }}
                  label='Mes'
                  mode='outlined'
                  keyboardType='number-pad'
                  value={card.exp_month}
                  onChangeText={text => { this.setState({ card: { ...card, exp_month: text } }); }}
                />
                <TextInput
                  style={{ backgroundColor: '#fff', width: '20%' }}
                  label='Año'
                  mode='outlined'
                  keyboardType='number-pad'
                  value={card.exp_year}
                  onChangeText={text => { this.setState({ card: { ...card, exp_year: text } }); }}
                />
              </View>
              <View style={{ paddingVertical: 10 }}>
                <Button loading={loading} disabled={loading} onPress={this.addCard} mode='contained'>Agregar</Button>
              </View>
            </View>
          )}
          {payMethod === 2 && (
            <View>
              <Text>PAYPAL</Text>
            </View>
          )}
        </View>
        <View style={{ paddingVertical: 30 }}>
          {payMethods.length < 1 &&
            <Text style={{ textAlign: 'center' }}>No hay métodos agregados</Text>
          }
          {
            payMethods.map((pm, i) => {
              if (pm.type === 1)
                return (
                  <View key={i} style={{ marginBottom: 20 }}>
                    <CardMethod label={`TARJETA ${pm.number}`} onPress={() => { }} />
                    <TouchableHighlight onPress={() => { this.deleteMethod(pm) }} underlayColor='#eee' style={{ position: 'absolute', right: 0, bottom: -14, paddingVertical: 2, paddingHorizontal: 5 }}>
                      <Text style={{ fontWeight: 'bold', color: '#c92724' }}>ELIMINAR</Text>
                    </TouchableHighlight>
                  </View>
                )
            })
          }
          {/* <View style={{ marginBottom: 20 }}>
            <PaypalMethod label='PAYPAL' onPress={() => { }} />
            <TouchableHighlight onPress={() => { }} underlayColor='#eee' style={{ position: 'absolute', right: 0, bottom: -14, paddingVertical: 2, paddingHorizontal: 5 }}>
              <Text style={{ fontWeight: 'bold', color: '#c92724' }}>ELIMINAR</Text>
            </TouchableHighlight>
          </View>
          <View style={{ marginBottom: 20 }}>
            <CardMethod label='VISA X-4000' onPress={() => { }} />
            <TouchableHighlight onPress={() => { }} underlayColor='#eee' style={{ position: 'absolute', right: 0, bottom: -14, paddingVertical: 2, paddingHorizontal: 5 }}>
              <Text style={{ fontWeight: 'bold', color: '#c92724' }}>ELIMINAR</Text>
            </TouchableHighlight>
          </View> */}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 15,
  },
});

export default withTheme(Pagos);