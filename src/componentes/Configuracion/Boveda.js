import React, { Component } from 'react';
import { StyleSheet, Text, View, AsyncStorage, ScrollView, TouchableHighlight } from 'react-native';
import { IconButton } from 'react-native-paper';
import { withTheme } from 'react-native-paper';
import Icon from 'react-native-vector-icons/FontAwesome5';
import * as DocumentPicker from 'expo-document-picker';
import { Linking } from 'expo';
import Storage from '../Media/Storage'
import Loading from '../Loading'
import { Http } from '../..//Config';


class Boveda extends Component {
	state = {
		vault: [],
		loading: true,
	}

	constructor() {
		super();
	}

	componentDidMount() {
		this.CargarDocumentos();
	}

	CargarDocumentos = async () => {
		this.setState({ loading: true })
		this.RT = await AsyncStorage.getItem('rt');
		var Res = await Http.post('vault/files', { rt: this.RT });
		// console.log(Res.data);
		this.setState({ vault: Res.data, loading: false })
	}

	document = async () => {
		var Document = await DocumentPicker.getDocumentAsync();
		// console.log(Document);
		var Subir = await Storage.Subir(Document);
		console.log(Subir.uri);
		var Res = await Http.post('vault/file', { file: Subir, rt: this.RT });
		console.log(Res);
		if (!Res.error) {
			this.setState({ vault: Res.data })
			// this.props.getUser(this.RT);
			// this.setState({ avatarLoading: false });
		}

	}

	render() {
		const { vault, loading } = this.state;
		return (
			<View style={styles.container}>
				<Loading loading={loading} />
				<ScrollView>
					{vault.map((file, i) => {
						return (
							<View key={i} style={{ margin: 10, elevation: 1, borderRadius: 10, backgroundColor: this.props.theme.colors.background }}>
								<TouchableHighlight onPress={() => { Linking.openURL(file.v_url); }} style={{ padding: 10 }} underlayColor={'#eee'}>
									<Text>Archivo {file.v_name} - Ver</Text>
								</TouchableHighlight>
							</View>
						)
					})}
				</ScrollView>

				<View style={[styles.add]}>
					<IconButton
						icon={({ size, color }) => (<Icon name="file" color={color} size={size} />)}
						color={'#ededed'}
						style={{ backgroundColor: this.props.theme.colors.primary, borderRadius: 25, width: 50, height: 50, elevation: 2 }}
						size={25}
						onPress={() => { this.document() }}
					/>
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},

	add: {
		position: 'absolute',
		bottom: 10,
		right: 10,
	}
});

export default withTheme(Boveda);