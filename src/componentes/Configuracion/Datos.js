import React, { Component, useState } from "react";
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  TouchableHighlight,
  AsyncStorage,
  BackHandler
} from "react-native";
import { withTheme, Button, Switch } from 'react-native-paper';
import { connect } from 'react-redux'
import { getUserByL, getUser, saveUser } from '../../store/Actions'
import { ConfigTitulo, ConfigSeparador, ConfigOpcion } from './Configuracion';
import Firebase from '../../Firebase';
import ByL from '../../ByL';
import Loading from '../Loading'
import Pop from '../Pop'

const CEditar = (props) => {
  const [value, setValue] = useState(props.value);

  return (
    <Pop height={70} onClose={() => { }}>
      <View style={styles.editar_bloque}>
        {props.children}
        {/* <TextInput style={styles.editar_input} mode='flat' placeholder={props.label} label={props.label} autoFocus={true} value={value} onChangeText={text => setValue(text)} />
        <Button style={styles.editar_button}
          mode="outlined"
          onPress={() => { props.onSave(value) }}
        >
          Guardar
        </Button> */}
      </View>
    </Pop>
  );
}
const ConfigEditar = withTheme(CEditar);

class Datos extends Component {

  state = {
    client: {
      name: '',
      email: '',
      birthday: null,
      gender: '',
      phone: null
    },
    edicion: {
      tutor_name: false,
      tutor_email: false,
      tutor_whatsapp: false,
      tutor_phone: false,
      tutor_app: false,
      value: null,
    }
  }

  componentDidMount() {
    this.CargarDatos();

    // setInterval(() => {
    //   this.setState({ visible: !this.state.visible })
    // }, 2000);
  }

  componentWillMount() {
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.CerrarPop);
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }

  CargarDatos = async () => {
    var rt = await AsyncStorage.getItem('rt');
    // var User = await Firebase.firestore()
    // 	.collection('users')
    // 	.doc(UID)
    //   .get();
    // console.log(User.data())
    // var User = Firebase.auth().currentUser;
    // console.log(User.email)
    // var Sales = await ByL.Datos(User.email);
    // console.log(Sales)
    // this.setState({ client: { ...Sales.client } })

    // console.log(this.props.user)
    // this.props.getUser(rt);
    this.props.getUserByL(rt);
  }

  CerrarPop = () => {
    // console.log(tutor_name)
    const { tutor_name, tutor_email, tutor_whatsapp, tutor_phone, tutor_app } = this.state.edicion;
    if (tutor_name || tutor_email || tutor_whatsapp || tutor_phone || tutor_app) {
      this.setState({
        edicion: {
          tutor_name: false,
          tutor_email: false,
          tutor_whatsapp: false,
          tutor_phone: false,
          tutor_app: false,
        }
      })
      return true;
    }
    return false;
  }

  Editar = async (que, valor) => {
    console.log(que, valor);
    try {
      var RT = await AsyncStorage.getItem('rt');
      this.props.saveUser(RT, { [que]: valor });
      this.setState({ edicion: { [que]: null } })
    }
    catch (e) {
      console.log(e)
    }
  }

  render() {
    // const { colors } = this.props.theme;
    // const { client } = this.state;
    const { byl } = this.props.user;
    const { edicion } = this.state;
    const { loading, user } = this.props;
    // console.log(edicion);

    var BD = new Date(byl.birthday);
    BD = `${('0' + (BD.getDate())).slice(-2)}-${('0' + (BD.getMonth() + 1)).slice(-2)}-${BD.getFullYear()}`;
    if(!byl.birthday) BD = null;
    console.log(user)
    return (
      <View style={styles.container}>
        <Loading loading={loading} />
        <ConfigTitulo titulo='Personales' />
        <ConfigOpcion nombre='Nombre'>{byl.name}</ConfigOpcion>
        <ConfigOpcion nombre='Correo electrónico'>{byl.email}</ConfigOpcion>
        <ConfigOpcion nombre='Fecha de nacimiento'>{BD}</ConfigOpcion>

        <ConfigTitulo titulo='Tutor' />
        <ConfigOpcion nombre='Nombre' onPress={() => { this.setState({ edicion: { tutor_name: true, value: user.tutor_name } }) }}>{user.tutor_name}</ConfigOpcion>
        <ConfigOpcion nombre='Correo electrónico' onPress={() => { this.setState({ edicion: { tutor_email: true, value: user.tutor_email } }) }}>{user.tutor_email}</ConfigOpcion>
        <ConfigOpcion nombre='WhatsApp' onPress={() => { this.setState({ edicion: { tutor_whatsapp: true, value: user.tutor_whatsapp } }) }}>{user.tutor_whatsapp}</ConfigOpcion>
        <ConfigOpcion nombre='Teléfono' onPress={() => { this.setState({ edicion: { tutor_phone: true, value: user.tutor_phone } }) }}>{user.tutor_phone}</ConfigOpcion>
        <ConfigOpcion nombre='Acceso a la app' onPress={() => { this.setState({ edicion: { tutor_app: true, value: user.tutor_app.toString() } }) }}>{user.tutor_app > 0 ? 'Sí' : 'No'}</ConfigOpcion>

        {edicion.tutor_name &&
          <ConfigEditar>
            <TextInput style={styles.editar_input} mode='flat' placeholder={'Nombre'} label={'Nombre'} autoFocus={true} value={edicion.value} onChangeText={text => this.setState({ edicion: { ...this.state.edicion, value: text, } })} />
            <Button style={styles.editar_button}
              mode="outlined"
              onPress={() => { this.Editar('tutor_name', edicion.value) }}
            >
              Guardar
            </Button>
          </ConfigEditar>
        }

        {edicion.tutor_email &&
          <ConfigEditar>
            <TextInput keyboardType='email-address' style={styles.editar_input} mode='flat' placeholder={'Nombre'} label={'Nombre'} autoFocus={true} value={edicion.value} onChangeText={text => this.setState({ edicion: { ...this.state.edicion, value: text, } })} />
            <Button style={styles.editar_button}
              mode="outlined"
              onPress={() => { this.Editar('tutor_email', edicion.value) }}
            >
              Guardar
            </Button>
          </ConfigEditar>
        }

        {edicion.tutor_whatsapp &&
          <ConfigEditar>
            <TextInput keyboardType='number-pad' style={styles.editar_input} mode='flat' placeholder={'Nombre'} label={'Nombre'} autoFocus={true} value={edicion.value} onChangeText={text => this.setState({ edicion: { ...this.state.edicion, value: text, } })} />
            <Button style={styles.editar_button}
              mode="outlined"
              onPress={() => { this.Editar('tutor_whatsapp', edicion.value) }}
            >
              Guardar
            </Button>
          </ConfigEditar>
        }

        {edicion.tutor_phone &&
          <ConfigEditar>
            <TextInput keyboardType='number-pad' style={styles.editar_input} mode='flat' placeholder={'Nombre'} label={'Nombre'} autoFocus={true} value={edicion.value} onChangeText={text => this.setState({ edicion: { ...this.state.edicion, value: text, } })} />
            <Button style={styles.editar_button}
              mode="outlined"
              onPress={() => { this.Editar('tutor_phone', edicion.value) }}
            >
              Guardar
            </Button>
          </ConfigEditar>
        }

        {edicion.tutor_app &&
          <ConfigEditar>
            <Switch
              color={this.props.theme.colors.primary}
              style={{width: '30%', marginRight: 50}}
              value={edicion.value > 0}
              onValueChange={() => { this.setState({ edicion: { ...this.state.edicion, value: edicion.value > 0 ? 0 : 1, } }) }
              }
            />
            <Button style={styles.editar_button}
              mode="outlined"
              onPress={() => { this.Editar('tutor_app', edicion.value) }}
            >
              Guardar
            </Button>
          </ConfigEditar>
        }

        {/* {edicion.tutor_email &&
          <ConfigEditar label='Correo electrónico del tutor' value={user.tutor_email} onSave={(val) => { this.Editar('tutor_email', val) }} />
        }
        {edicion.tutor_whatsapp &&
          <ConfigEditar label='WhatsApp del tutor' value={user.tutor_whatsapp} onSave={(val) => { this.Editar('tutor_whatsapp', val) }} />
        }
        {edicion.tutor_phone &&
          <ConfigEditar label='Teléfono del tutor' value={user.tutor_phone} onSave={(val) => { this.Editar('tutor_phone', val) }} />
        }
        {edicion.tutor_app &&
          <ConfigEditar label='Acceso a la App' value={user.tutor_app} onSave={(val) => { this.Editar('tutor_app', val) }} />
        } */}

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  editar_bloque: {
    flexDirection: 'row'
  },
  editar_input: {
    width: '67%',
    marginRight: 5,
    backgroundColor: '#fff'
  },
  editar_button: {
    // width: '25%',
    marginTop: 5,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center'
  },


  titulo_bloque: {
    paddingHorizontal: 10,
    paddingTop: 6,
    paddingBottom: 1,
  },
  titulo_nombre: {
    fontWeight: "bold",
    fontSize: 15,
    color: '#fff'
  },
});


const mapStateToProps = (state) => {
  return {
    user: state.user,
    loading: state.loading
  };
}
const mapDispatchToProps = (dispatch) => {
  return {
    getUser: (uid) => dispatch(getUser(uid)),
    saveUser: (uid, data) => dispatch(saveUser(uid, data)),
    getUserByL: (email) => dispatch(getUserByL(email))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(Datos));

// export default withTheme(Datos);