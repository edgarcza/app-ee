import React, { Component } from 'react';
import { StyleSheet, View, KeyboardAvoidingView, AsyncStorage, Text, Alert } from 'react-native';
import { TextInput, Button, ActivityIndicator } from 'react-native-paper';
import { withNavigation } from 'react-navigation';
import { withTheme } from 'react-native-paper';
// import axios from 'axios';
import Firebase from '../Firebase';
import ByL from '../ByL';
import { Http } from '../Config'
import 'firebase/auth';
import 'firebase/firestore';

class Registro extends Component {
	static navigationOptions = {
		title: 'Registro',
	};
	constructor() {
		super();

		this.state = {
			// email: 'ejimenez@estudiantesembajadores.com',
			email: 'eder_josimar@hotmail.com',
			password: '123456',
			password2: '123456',
			validado: true,
			cargando: false,
		}
	}

	async Registro() {
		// var Req = axios.get('')


		if (this.state.password !== this.state.password2)
			return alert('Las contraseñas no son iguales')
		this.setState({ cargando: true });

		try {
			var ByLEs = new ByL(this.state.email);
			if ((await ByLEs.Buscar())) {
				try {
					var Estudiante = ByLEs.estudiante;

					var Req = await Http.post('user/create', {
						...Estudiante,
						...this.state
					});
					console.log(Req);
					if (!Req.error) {
						alert('Usuario registrado');
						// AsyncStorage.setItem('uid', res.user.uid);
						this.props.navigation.navigate('Login')
					}
					else {
						alert('Error al crear usuario: ' + Req.errorMessage);
						Alert.alert('Error', Req.errorMessage)
					}
					this.setState({ cargando: false });
				} catch (error) {
					alert('Error al crear usuario');
					console.log(error.code, error.message)
					this.setState({ cargando: false });
				}
			}
			else
				return alert('El correo no tiene un programa con Estudiantes Embajadores');
		} catch (e) {
			console.log('Error', e);
			this.setState({ cargando: false });
		}
	}

	Validar() {
		if (this.state.email !== "" && this.state.password !== "" && this.state.password2 !== "")
			if (this.state.password.length >= 6 && this.state.password2.length >= 6)
				if (this.state.password === this.state.password2)
					this.setState({ validado: true })
	}

	render() {
		return (
			<View style={styles.container}>
				<KeyboardAvoidingView
					behavior='padding'
				// keyboardVerticalOffset={Header.HEIGHT + 70}
				// enabled
				// style={{ flex: 1 }}
				>
					<View style={{ padding: 25 }}>
						<View>
							{/* <Text style={{
							fontWeight: 'bold',
							fontSize: 30,
							textAlign: 'center',
							marginVertical: 10,
							color: this.props.theme.colors.primary
						}}>REGISTRO</Text> */}
						</View>
						<View style={{ margin: 10 }}>
							<TextInput
								label='Correo electrónico'
								value={this.state.email}
								keyboardType={'email-address'}
								onChangeText={text => { this.setState({ email: text }); this.Validar() }}
							/>
						</View>
						<View style={{ margin: 10 }}>
							<TextInput
								secureTextEntry={true}
								label='Contraseña'
								value={this.state.password}
								onChangeText={text => { this.setState({ password: text }); this.Validar() }}
							/>
						</View>
						<View style={{ margin: 10 }}>
							<TextInput
								secureTextEntry={true}
								label='Confirmar contraseña'
								value={this.state.password2}
								onChangeText={text => { this.setState({ password2: text }); this.Validar() }}
							/>
						</View>
						<View style={{ margin: 10 }}>
							<Button
								mode="contained"
								onPress={() => { this.Registro(); }}
								disabled={!this.state.validado || this.state.cargando}
								loading={this.state.cargando}
							>
								<Text>Registrarse</Text>
							</Button>
						</View>
					</View>
				</KeyboardAvoidingView>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		// backgroundColor: '#00b7ff',
		// alignItems: 'center',
		justifyContent: 'center',
	},
});

export default withNavigation(withTheme(Registro));
// export default Registro;