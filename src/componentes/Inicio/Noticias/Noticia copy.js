import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { Button, Card, Title, Paragraph } from 'react-native-paper';
import { withTheme } from 'react-native-paper';
import Icon from 'react-native-vector-icons/FontAwesome';

class Noticia extends Component {
  constructor() {
    super();
  }

  render() {
    const {colors} = this.props.theme;
    const {datos} = this.props;
    return (
      <Card style={{ backgroundColor: colors.background, margin: 5, borderRadius: 15 }}>
        <Card.Content>
          <Title>{datos.title}</Title>
          <Paragraph>{datos.content}</Paragraph>
        </Card.Content>
        <Card.Cover source={{ uri: datos.image }} />
        <Card.Actions>
          <Button icon={<Icon name="heart" size={20} color={'#e6382c'} />} onPress={() => { }}>Me gusta</Button>
        </Card.Actions>
      </Card>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: Tema.colors.accent,
  },
});

export default withTheme(Noticia);
// export default Noticias;