import React, { Component } from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import { Button, Card, Title, Paragraph } from 'react-native-paper';
import { withTheme } from 'react-native-paper';
import Icon from 'react-native-vector-icons/FontAwesome';

const NComp = (props) => {
  const { data } = props;
  return (
    <View style={[styles.n_caja, { backgroundColor: props.theme.colors.background }]}>
      <View style={{ flexDirection: 'row' }}>
        <View>
          <Image source={{ uri: data.image }} style={{ width: 100, height: 100 }} />
        </View>
        <View style={{ paddingTop: 4, width: '70%', backgroundColor: '#eee', justifyContent: 'center', alignContent: 'center', alignItems: 'center' }}>
          <Text>{data.content_5}</Text>
          <Text style={{fontWeight: 'bold'}}>{data.content_4.charAt(0).toUpperCase() + data.content_4.slice(1)}</Text>
          <Text>{data.content}° (sensación de {data.content_2}°)</Text>
          <Text>{data.content_3}% de humedad</Text>
        </View>
      </View>
    </View>
  )
}
export const NoticiaClima = withTheme(NComp);

const NLComp = (props) => {
  return (
    <View style={[styles.n_caja, { backgroundColor: props.theme.colors.background }]}>
      <View style={{ flexDirection: 'row' }}>
        <View>
          <Image source={{ uri: 'https://i.imgur.com/V1OwEQA.png' }} style={{ width: 200, height: 200, resizeMode: 'contain' }} />
        </View>
        <View style={{ paddingTop: 4, justifyContent: 'center' }}>
          <Text style={{ fontSize: 20, fontWeight: 'bold' }}>El lugar</Text>
          <Text>Descripción del lugar</Text>
        </View>
      </View>
    </View>
  )
}
export const NoticiaLugar = withTheme(NLComp);

class Noticia extends Component {
  constructor() {
    super();
  }

  render() {
    const { colors } = this.props.theme;
    const { datos } = this.props;

    if (datos.type == 1)
      return (
        <NoticiaClima data={datos} />
      );

    return (<Text>Error</Text>)
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: Tema.colors.accent,
  },

  n_caja: {
    margin: 10,
    borderRadius: 15,
    elevation: 2,
  }
});

export default withTheme(Noticia);
// export default Noticias;