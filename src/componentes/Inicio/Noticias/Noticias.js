import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, FlatList, AsyncStorage } from 'react-native';
import { Avatar, Button, Card, Title, Paragraph } from 'react-native-paper';
import { withTheme } from 'react-native-paper';
import HeaderEE from '../../Header';
import Noticia, { NoticiaClima, NoticiaLugar } from './Noticia';
import { Http } from '../../../Config';
// import Firebase from '../../../Firebase'

class Noticias extends Component {
	state = {
		noticias: [
			// {id: 1, type: 1, content: ''}
		],
		cargando: false,
	}

	constructor() {
		super();
	}

	componentDidMount() {
		this.CargarNoticias();
	}

	CargarNoticias = async () => {
		this.setState({cargando: true});
		var RT = await AsyncStorage.getItem('rt');
		var Res = await Http.post('infos/user', {rt: RT});
		// console.log(Res.data);
		if(!Res.error) {
			this.setState({noticias: Res.data, cargando: false});
		}

		// this.setState({cargando: true});
		// var NoticiasDocs = await Firebase.firestore()
		// 	.collection('news')
		// 	.orderBy('timestamp')
		// 	.get();
		// var Noticias = NoticiasDocs.docs.map((doc) => {
		// 	var not = doc.data();
		// 	not.id = doc.id;
		// 	return not;
		// });
		// // console.log(Noticias)
		// this.setState({noticias: Noticias, cargando: false});
	}

	render() {
		const { noticias } = this.state;
		return (
			<View style={[styles.container, {backgroundColor: this.props.theme.colors.backdrop}]}>
				<HeaderEE></HeaderEE>
				<FlatList
					data={noticias}
					renderItem={({ item }) => <Noticia datos={item} />}
					keyExtractor={noticia => noticia.id.toString()}
					onEndReachedThreshold={0.1}
					// onEndReached={() => { this.CargarNoticias() }}
					refreshing={this.state.cargando}
					onRefresh={() => this.CargarNoticias()}
				/>
				{/* <NoticiaClima />
				<NoticiaLugar /> */}
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},

	caja: {
		marginBottom: 10,
		backgroundColor: '#fff',
	},

	media: {
		resizeMode: 'cover',
	},

	titulo: {
		fontWeight: "bold",
		fontSize: 20,
		padding: 5,
	},

	descripcion: {
		padding: 5,
	}
});

export default withTheme(Noticias);
// export default Noticias;