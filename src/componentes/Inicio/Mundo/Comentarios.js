import React, { Component } from 'react';
import {
	StyleSheet,
	Text,
	View,
	FlatList,
	AsyncStorage
} from 'react-native';
import { withTheme, Avatar, ActivityIndicator } from 'react-native-paper';
import Firebase from '../../../Firebase'
import { Http } from '../../../Config';

class Comentario extends Component {
	
	render() {
		const {datos} = this.props;
		return (
			<View style={[styles.c_comentario, { backgroundColor: this.props.theme.colors.background }]}>
				<Avatar.Image size={40} source={{uri: datos.avatar}} />
				<View style={[styles.c2_comentario]}>
					<Text style={[{ fontWeight: 'bold', color: this.props.theme.colors.text }]}>{datos.name}</Text>
					<Text style={[{ color: this.props.theme.colors.text }]}>{datos.pc_comment}</Text>
				</View>
			</View>
		);
	}
}
const ComentarioComp = withTheme(Comentario);

class Comentarios extends Component {
	state = {
		cargando: true,
		comentarios: null,
	}
	constructor() {
		super();
	}

	componentDidMount() {
		this.CargarComentarios();
	}

	CargarComentarios = async () => {
		// console.log(this.props.navigation.state)
		this.setState({cargando: true});
		const { pib } = this.props.navigation.state.params;
		try {
			var rt = await AsyncStorage.getItem('rt');
			var Req = await Http.post('post/comments', {rt: rt, post_id: pib});
			if(!Req.error) {
				this.setState({comentarios: Req.data, cargando: false});
			}
		}
		catch(e) {
			console.log(e)
		}

		// const ComReq = await Firebase.firestore()
		// 	.collection('posts')
		// 	.doc(pib)
		// 	.collection('comments')
		// 	.get();
		// var Coms = ComReq.docs.map((doc) => doc.data())
		// console.log(Coms)
		// this.setState({comentarios: Coms, cargando: false});
	}

	render() {
		const {comentarios} = this.state;
		return (
			<View style={styles.container}>
				<FlatList
					data={comentarios}
					renderItem={({ item }) => <ComentarioComp datos={item} />}
					keyExtractor={(comentario, i) => comentario.main_id.toString()}
					onEndReachedThreshold={0.1}
					refreshing={this.state.cargando}
					onRefresh={() => { this.CargarComentarios() }}
				/>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		// alignItems: 'center',
		// justifyContent: 'center',
	},
	c_comentario: {
		flexDirection: 'row',
		paddingVertical: 8,
		paddingHorizontal: 10,
		borderBottomWidth: 1,
		borderBottomColor: '#eee'
	},
	c2_comentario: {
		paddingLeft: 8,
	}
});

export default withTheme(Comentarios);