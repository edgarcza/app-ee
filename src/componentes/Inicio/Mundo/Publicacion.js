import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableHighlight,
  AsyncStorage,
  FlatList,
  Image,
  ScrollView,
  TextInput,
  KeyboardAvoidingView
} from 'react-native';
// import AutoHeightImage from 'react-native-auto-height-image';
import { withTheme, Avatar } from 'react-native-paper';
import { withNavigation } from 'react-navigation'
import Icon from 'react-native-vector-icons/FontAwesome';
import Firebase from '../../../Firebase'
import { Tema, Http } from '../../../Config';

class Publicacion extends Component {
  constructor(props) {
    super();
    this.state = {
      likes: props.datos.likes,
      liked: props.datos.liked,
      comentarios: props.datos.comentarios,
      comentar: null,
    }
  }

  componentDidMount() {
    this.setState({
      liked: this.props.datos.liked,
      post_likes: this.props.datos.post_likes,
      post_comments: this.props.datos.post_comments,
    });
    // this.CargarDatos();
  }

  componentWillUnmount() {
    // this.unSub();
  }

  async CargarDatos() {
    // this.unSub = Firebase.firestore()
    //   .collection('posts')
    //   .doc(this.props.datos.id)
    //   .onSnapshot((datos) => {
    //     this.setState({
    //       likes: datos.data().likes,
    //       comentarios: datos.data().comments,
    //     })
    //   })
  }

  async DarLike() {
    // console.log('like a', this.props.datos);
    const { liked } = this.state;

    try {
      const UID = await AsyncStorage.getItem('rt');
      this.setState({ liked: !liked });
      Http.post('post/like', { rt: UID, post_id: this.props.datos.main_id })
        .then(res => {
          if (!res.error) {
            this.setState({ post_likes: res.data.post_likes });
          }
        })
        .catch((e) => {
          this.setState({ liked: !liked })
        });
    }
    catch (e) {
      console.log(e);
    }
  }

  Comentar() {
    // console.log(this.props)
    this.props.comentar(this.props.datos.main_id, this.Comentado);
  }

  Comentado = (data) => {
    console.log(data);
    this.setState({ post_comments: data.post_comments });
  }

  VerComentarios = () => {
    // console.log(this.props.navigation)
    this.props.navigation.navigate('Comentarios', { pib: this.props.datos.main_id });
  }

  render() {
    const { colors } = this.props.theme;
    const { datos, profile } = this.props;
    const { post_likes, post_comments, liked } = this.state;
    // console.log(this.props);
    return (
      <View style={[styles.caja, { backgroundColor: colors.background }]}>
        {!profile &&
          <View style={[styles.pub_por_v, {borderBottomWidth: 1, borderBottomColor: '#eee' }]}>
            <Avatar.Image style={styles.avatar} size={30} source={{ uri: datos.avatar }} />
            <Text style={[styles.pub_por_n, { color: colors.text }]}>{datos.name}</Text>
          </View>
        }
        {/* <View style={[{ padding: 5, borderTopWidth: 1, borderTopColor: '#eee' }]}> */}
        <View style={[{ padding: 5 }]}>
          <Text style={[styles.pub_texto, { color: colors.text }]}>{datos.post_content}</Text>
        </View>
        <View style={{}}>
          <ScrollView
            style={{ borderRadius: 20 }}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}>
            {datos.media.map((item, i) => {
              return <Image key={item.id} source={{ uri: item.pm_url }} style={{ width: Dimensions.get('window').width, height: 400, resizeMode: 'cover', marginLeft: 3, alignSelf: 'center' }} />
            })}
          </ScrollView>
        </View>
        <View style={{ flexDirection: 'row', justifyContent: 'flex-end', borderTopWidth: 1, borderTopColor: '#eee', paddingHorizontal: 20, paddingVertical: 5, }}>
          <View style={[styles.pub_acciones]}>
            <TouchableHighlight onPress={() => { this.DarLike() }} underlayColor='#f5ccc9'
              style={[
                styles.pub_icon,
                { borderColor: '#f5ccc9' },
                (liked) ? { backgroundColor: '#f5ccc9' } : null
              ]}>
              <Icon name="heart" size={20} color={'#e6382c'} />
            </TouchableHighlight>
            <TouchableHighlight onPress={() => { }} underlayColor='#f5ccc9' style={[styles.pub_icon_cantidad, { borderColor: '#f5ccc9' }]}>
              <Text style={[styles.pub_acciones_text, { color: '#e6382c' }]}>{post_likes}</Text>
            </TouchableHighlight>
          </View>
          <View style={[styles.pub_acciones]}>
            <TouchableHighlight onPress={() => { this.Comentar() }} underlayColor='#d0dff2' style={[styles.pub_icon, { borderColor: '#d0dff2' }]}>
              <Icon name="comment" size={20} color={'#3d72b8'} />
            </TouchableHighlight>
            <TouchableHighlight onPress={() => this.VerComentarios()} underlayColor='#d0dff2' style={[styles.pub_icon_cantidad, { borderColor: '#d0dff2' }]}>
              <Text style={[styles.pub_acciones_text, { color: '#3d72b8' }]}>{post_comments}</Text>
            </TouchableHighlight>
          </View>
        </View>

        {/* <View style={[styles.c_comentar]}>
          <View style={[styles.comentar]}>
            <TextInput
              label='Comentario...'
              multiline={true}
              value={this.state.comentar}
              onChangeText={text => { this.setState({ comentar: text }); }}
            />
          </View>
        </View> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // alignItems: 'center',
    // justifyContent: 'center',
    backgroundColor: Tema.colors.accent,
  },

  caja: {
    marginBottom: 10,
    backgroundColor: Tema.colors.background,
    borderRadius: 15,
    marginHorizontal: 5,
    elevation: 1,
  },

  pub_texto: {
    paddingHorizontal: 15,
    paddingTop: 10,
    paddingBottom: 7,
  },
  pub_media: {
    width: '100%',
    height: undefined,
    aspectRatio: 1,
    resizeMode: 'contain',
    flex: 1,
    borderRadius: 20,
  },
  pub_por_v: {
    paddingTop: 15,
    paddingHorizontal: 15,
    paddingBottom: 5,
    flexDirection: 'row',
    alignItems: 'center',
  },
  pub_por_n: {
    fontWeight: 'bold',
    marginLeft: 20
  },
  pub_acciones: {
    flexDirection: 'row',
    marginLeft: 10,
  },
  pub_acciones_text: {
    textAlign: 'center',
  },

  pub_icon: {
    paddingVertical: 5,
    paddingHorizontal: 8,
    borderTopLeftRadius: 8,
    borderBottomLeftRadius: 8,
    borderWidth: 1,
    borderRightWidth: 0,
  },
  pub_icon_cantidad: {
    paddingVertical: 5,
    paddingHorizontal: 8,
    borderTopRightRadius: 8,
    borderBottomRightRadius: 8,
    borderWidth: 1,
    borderColor: '#e6382c',
  }
});

Publicacion.defaultProps = {
  profile: false,
}

export default withNavigation(withTheme(Publicacion));