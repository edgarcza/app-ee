import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableHighlight } from 'react-native';
import { withTheme, Avatar, ActivityIndicator } from 'react-native-paper';
import { withNavigation } from 'react-navigation';
import { Tema } from '../../../Config';


class Publicar extends Component {
  constructor(props) {
    super();
    // console.log('as', props);
  }

  render() {
    return (
      <View>
        <View style={{
          ...styles.caja,
          flexDirection: 'row',
          borderTopColor: Tema.colors.accent,
          borderTopWidth: 0,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: this.props.theme.colors.background,
          borderBottomLeftRadius: 15,
          borderBottomRightRadius: 15,
          elevation: 1
        }}>
          <Avatar.Image style={styles.avatar} size={30} source={require('../../../../assets/avatar.png')} />
          <TouchableHighlight style={styles.qep} onPress={() => { this.props.navigation.navigate('Crear'); }} underlayColor='#ededed'>
            <Text style={{ color: this.props.theme.colors.text }}>Publicar...</Text>
          </TouchableHighlight>
        </View>
        {this.props.cargando &&
          <View style={{ marginBottom: 8 }}>
            <ActivityIndicator animating={true} size={40} color={this.props.theme.colors.primary} />
          </View>
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // alignItems: 'center',
    // justifyContent: 'center',
    backgroundColor: Tema.colors.accent,
  },

  caja: {
    marginBottom: 10,
    backgroundColor: Tema.colors.background,
  },

  qep: {
    width: '80%',
    borderWidth: 1,
    borderColor: "#adadad",
    marginVertical: 10,
    marginHorizontal: 15,
    paddingVertical: 10,
    paddingHorizontal: 15,
    borderRadius: 15,
  },
});

export default withNavigation(withTheme(Publicar));