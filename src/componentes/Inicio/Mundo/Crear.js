import React, { Component } from 'react';
import { StyleSheet, Text, View, TextInput, TouchableHighlight, KeyboardAvoidingView, AsyncStorage, Image, FlatList } from 'react-native';
import { Button } from 'react-native-paper';
import * as ImageManipulator from 'expo-image-manipulator';
import { withTheme, ActivityIndicator } from 'react-native-paper';
import * as ImagePicker from 'expo-image-picker';
import Icon from 'react-native-vector-icons/FontAwesome';
import Firebase from '../../../Firebase';
import 'firebase/storage';
// const uuidv1 = require('uuid/v4');
import uuidv4 from 'uuid/v4'
import { Http } from '../../../Config';
// import uuid from 'react-native-uuid'
// import RNFetchBlob from "react-native-fetch-blob";


class Crear extends Component {
	static navigationOptions = ({ navigation }) => {
		const { params = {} } = navigation.state;
		return {
			headerRight:
				(<TouchableHighlight style={{ marginRight: 10, padding: 5 }} onPress={() => params.publicar()} underlayColor="#ededed">
					<View>
						<Text style={{ fontSize: 16 }}>PUBLICAR</Text>
					</View>
				</TouchableHighlight>)
		};
	};

	state = {
		texto: '',
		validado: false,
		publicando: false,
		media: null,
		cargando: false,
	}

	constructor() {
		super();
	}

	componentDidMount() {
		this.props.navigation.setParams({ publicar: this.Publicar.bind(this) });

		const { navigation } = this.props;
		// this.focusListener = navigation.addListener('didFocus', () => {
		// 	if (this.props.navigation.state.params && this.props.navigation.state.params.media) {
		// 		this.setState({ media: this.props.navigation.state.params.assets });
		// 	}
		// });
	}

	Validar() {
		if (this.state.texto !== "")
			this.setState({ validado: true })
	}

	async Publicar() {
		// console.log(this.state.texto);
		const { validado, texto, media, cargando } = this.state;
		if (cargando) return;
		var mediaArray = [];
		this.setState({ cargando: true });
		// if (!validado) return;
		try {
			const UID = await AsyncStorage.getItem('rt');

			var mediaArray = []

			// console.log(media)
			if (media) {
				mediaArray = await Promise.all(media.map(async (media, i) => {
					var euuid = uuidv4();
					// console.log(media.uri + ' |||| ' + euuid);
					var FComp = await ImageManipulator.manipulateAsync(media.uri, [], { compress: 0.2, base64: true });
					// var FFetch = await fetch(FComp.uri);
					// var FBlob = await FFetch.blob();
					// console.log(FComp.)

					// var SubRef = Firebase.storage().ref(`images/posts/${euuid}.jpg`);
					// var Subida = await SubRef.put(FBlob);
					// var Url = await SubRef.getDownloadURL();

					return { id: euuid, uri: FComp.uri, base64: FComp.base64 }
				}));
			}
			// console.log(mediaArray)
			// console.log({
			// 	content: texto,
			// 	rt: UID,
			// 	date: new Date().toUTCString(),
			// 	timestamp: new Date(),
			// 	media: mediaArray
			// })
			var Req = await Http.post('post/create', {
				content: texto,
				rt: UID,
				date: new Date().toUTCString(),
				timestamp: new Date(),
				media: mediaArray
			});
			// console.log(Req);
			if (!Req.error) {
				this.props.navigation.navigate('Mundo', { recargar: true, creada: Crear });
				this.props.navigation.goBack()
			}

			// const CrearRef = Firebase.firestore().collection('posts').doc();
			// const Crear = await CrearRef.set({
			// 	content: texto,
			// 	user: { uid: UID },
			// 	date: new Date().toUTCString(),
			// 	timestamp: new Date(),
			// 	likes: 0,
			// 	comments: 0,
			// 	media: mediaArray
			// });

		} catch (error) {
			console.log(error);
		}
	}

	SubirMedia() {
		this.props.navigation.navigate('Media', { cb: this.SubidaMedia.bind(this) });
	}

	SubidaMedia(a) {
		if (a)
			this.setState({ media: a });
	}

	async SubirFoto() {
		try {
			let Subida = await ImagePicker.launchCameraAsync({
				allowsEditing: true,
			});
			console.log(Subida)
			if (!Subida.cancelled) {
				let nA = (!this.state.media) ? [] : this.state.media;
				nA.push(Subida);
				this.setState({ media: nA });
			}
		} catch (error) {
			console.log(error);
		}
	}

	render() {
		let { media, cargando } = this.state;
		return (
			<View style={[styles.container, { backgroundColor: this.props.theme.colors.background }]}>
				<KeyboardAvoidingView style={[{ flex: 1 }]} behavior='padding'>
					<TextInput
						style={[styles.text]}
						onChangeText={text => { this.setState({ texto: text }); this.Validar() }}
						value={this.state.texto}
						placeholder={'Escribe algo...'}
						multiline={true}
						textAlignVertical={'top'}
					/>
					{media &&
						<View style={[styles.media]}>
							<FlatList
								style={{ flex: 1 }}
								data={media}
								renderItem={({ item }) => { return <Image source={{ uri: item.uri }} style={{ width: 150, height: 150, resizeMode: 'cover', marginLeft: 3 }} /> }}
								keyExtractor={item => item.uri}
								horizontal={true}
								showsHorizontalScrollIndicator={false}
							/>
							{/* <Image source={{ uri: media }} style={[styles.media_previa]} /> */}
						</View>}
					<View style={[styles.extras]}>
						<TouchableHighlight style={[]} onPress={() => { this.SubirMedia() }} underlayColor="#ededed">
							<View style={[styles.extra]}>
								<Icon name="image" size={40} color={'#bdbdbd'} />
								{/* <Text style={[{ color: "#bdbdbd" }]}>IMAGEN</Text> */}
							</View>
						</TouchableHighlight>
						<TouchableHighlight style={[]} onPress={() => { this.SubirFoto() }} underlayColor="#ededed">
							<View style={[styles.extra]}>
								<Icon name="camera" size={40} color={'#bdbdbd'} />
								{/* <Text style={[{ color: "#bdbdbd" }]}>CÁMARA</Text> */}
							</View>
						</TouchableHighlight>
					</View>
				</KeyboardAvoidingView>
				{cargando &&
					<View style={{ justifyContent: 'center', position: 'absolute', height: '100%', width: '100%' }}>
						<View style={{ justifyContent: 'center', position: 'absolute', backgroundColor: '#000', height: '100%', width: '100%', opacity: 0.1 }}></View>
						<ActivityIndicator size={60} />
					</View>
				}
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},

	text: {
		// height: 100,
		flex: 5,
		width: '100%',
		fontSize: 20,
		padding: 7,
	},

	media: {
		flex: 4,
		justifyContent: 'center',
		alignItems: 'center'
	},
	media_previa: {
		width: 200,
		height: 200,
		resizeMode: 'contain'
	},

	extras: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'center'
	},

	extra: {
		alignItems: 'center',
		justifyContent: 'center',
		padding: 10,
	}
});

export default withTheme(Crear);