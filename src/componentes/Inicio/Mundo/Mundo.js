import React, { Component } from 'react';
import { StyleSheet, View, Text, FlatList, AsyncStorage, TextInput, KeyboardAvoidingView, Animated, BackHandler } from 'react-native';
import { withTheme, Avatar, IconButton, Button } from 'react-native-paper';
import { connect } from 'react-redux'
import { getUser } from '../../../store/Actions'
import HeaderEE from '../../Header';
import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';
import { Tema, Http } from '../../../Config';
import Firebase from '../../../Firebase';
import 'firebase/functions';
import Publicar from './Publicar';
import Publicacion from './Publicacion';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { GET_USER } from '../../../store/Types';
//probando 2
class Mundo extends Component {
	state = {
		publicaciones: [],
		cargando: true,

		comentar: null,
		comentario_validado: false,
		comentario_pub: null,
		comentar_anim: new Animated.Value(0)
	}

	constructor(props) {
		super();
		// RNPTema = props.theme.colors;
		// console.log(RNPTema);
		// this.CargarPublicaciones();
	}

	componentDidMount() {
		// console.log(this.props);

		AsyncStorage.getItem('rt')
			.then((uid) => {
				this.props.getUser(uid);
			});
		// console.log(this.props.user)

		// this.props.dispatch(GET_USER)

		this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.CerrarComentar);

		this.CargarPublicaciones();
		const { navigation } = this.props;
		this.focusListener = navigation.addListener('didFocus', () => {
			if (this.props.navigation.state.params && this.props.navigation.state.params.recargar) {
				this.CargarPublicaciones();
				delete this.props.navigation.state.params.recargar;
			}
		});

		this.Notificaciones();
	}

	componentWillUnmount() {
		this.backHandler.remove()
	}

	componentWillReceiveProps(props) {
		// console.log(props.user)
	}

	async Notificaciones() {
		const { status: existingStatus } = await Permissions.getAsync(
			Permissions.NOTIFICATIONS
		);
		let finalStatus = existingStatus;

		if (existingStatus !== 'granted') {
			const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
			finalStatus = status;
		}

		if (finalStatus !== 'granted') {
			return;
		}

		try {
			let token = await Notifications.getExpoPushTokenAsync();
			var UID = await AsyncStorage.getItem('rt');
			// console.log({ rt: UID, nToken: token });
			Http.post('user/device', { rt: UID, nToken: token }).catch(e => {});
		} catch (e) {
			console.log(e);
		}

		// Firebase.firestore()
		// 	.collection('users')
		// 	.doc(UID)
		// 	.update({
		// 		ntoken: token
		// 	});
	}

	CargarPublicaciones = async () => {
		// console.log('CargarPublicaciones');
		this.setState({ cargando: true });
		// let pubs = this.state.publicaciones;

		try {
			var rt = await AsyncStorage.getItem('rt');
			var Req = await Http.post('posts', { rt: rt });
			// console.log(Req.data);
			this.setState({ publicaciones: Req.data, cargando: false });

		} catch (error) {
			console.log(error);
		}

	}

	async Comentar() {
		const { comentario_pub, comentar } = this.state;
		// console.log(comentario_pub, comentar);
		try {
			const UID = await AsyncStorage.getItem('rt');
			var Req = await Http.post('post/comment', {
				post_id: comentario_pub,
				rt: UID,
				comment: comentar,
			});
			if (!Req.error) {
				this.Comentado(Req.data);
			}
		}
		catch (e) {
			console.log(e);
		}
		this.setState({ comentario_pub: null, comentar: null, comentar_anim: new Animated.Value(0) })

		// const Comentario = await Firebase.firestore()
		// 	.collection('posts')
		// 	.doc(comentario_pub)
		// 	.collection('comments')
		// 	.doc()
		// 	.set({
		// 		user: { uid: UID },
		// 		date: new Date().toUTCString(),
		// 		timesstamp: new Date(),
		// 		comment: comentar
		// 	});


		// this.setState({ comentario_pub: null, comentar: null, comentar_anim: new Animated.Value(0) })
	}

	AbrirComentar = (id, cb) => {
		// console.log('asd')
		this.setState({ comentario_pub: id })
		this.Comentado = cb;
		Animated.timing(
			this.state.comentar_anim, {
			toValue: 1,
			duration: 600,
		}
		).start()
	}

	CerrarComentar = () => {
		if (this.state.comentario_pub) {
			this.setState({ comentario_pub: null, comentar: null, comentar_anim: new Animated.Value(0) });
			return true;
		}
		return false;
	}

	render() {
		var { publicaciones, comentario_validado, comentario_pub, comentar_anim } = this.state;
		// console.log(publicaciones)
		return (
			<View style={[styles.container, { backgroundColor: this.props.theme.colors.backdrop, paddingTop: 10 }]}>
				<FlatList
					data={publicaciones}
					extraData={this.state}
					renderItem={({ item }) => <Publicacion datos={item} comentar={this.AbrirComentar} />}
					keyExtractor={(publicacion, i) => publicacion.main_id.toString()}
					onEndReachedThreshold={0.1}
					refreshing={this.state.cargando}
					onRefresh={() => { this.CargarPublicaciones() }}
				// onEndReached={() => { this.CargarPublicaciones() }}
				/>

				<View style={[styles.nueva]}>
					<IconButton
						icon={({ size, color }) => (<Icon name="comment" color={color} size={size} />)}
						color={'#ededed'}
						style={{ backgroundColor: this.props.theme.colors.primary, borderRadius: 25, width: 50, height: 50, elevation: 2 }}
						size={25}
						onPress={() => { this.props.navigation.navigate('Crear') }}
					/>
				</View>

				{comentario_pub &&
					<KeyboardAvoidingView behavior="padding" style={[styles.c_comentar]}>
						<Animated.View style={[styles.comentar, { opacity: comentar_anim }]}>
							<TextInput
								style={{ fontSize: 16, width: '67%', minHeight: 10 }}
								placeholder='Comentario...'
								multiline={true}
								value={this.state.comentar}
								autoFocus={true}
								onChangeText={text => { this.setState({ comentar: text }); if (text.length > 0) this.setState({ comentario_validado: true }) }}
							/>
							{/* <View style={{ flexDirection: 'row' }}> */}
							<Button
								style={{ marginTop: 5, borderRadius: 5, alignItems: 'center', justifyContent: 'center' }}
								labelStyle={{ alignItems: 'center' }}
								mode="outlined"
								onPress={() => { this.Comentar(); }}
								disabled={!comentario_validado}
							>
								Comentar
            		</Button>
						</Animated.View>
					</KeyboardAvoidingView>}
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		// alignItems: 'center',
		// justifyContent: 'center',
		// backgroundColor: Tema.colors.accent,
	},

	caja: {
		marginBottom: 10,
		backgroundColor: Tema.colors.background,
	},

	qep: {
		width: '80%',
		borderWidth: 1,
		borderColor: Tema.colors.primary,
		marginVertical: 10,
		marginHorizontal: 15,
		paddingVertical: 10,
		paddingHorizontal: 15,
		borderRadius: 20,
	},

	pub_texto: {
		paddingHorizontal: 15,
		paddingTop: 10,
		paddingBottom: 7,
	},
	pub_media: {
		width: '100%',
		height: undefined,
		aspectRatio: 1,
		// height: null,
		resizeMode: 'contain',
		flex: 1,
		// justifyContent: 'flex-end',
		// padding: 0,
		// backgroundColor: '#fff'
	},
	pub_por_v: {
		paddingTop: 15,
		paddingHorizontal: 15,
		flexDirection: 'row',
		alignItems: 'center',
	},
	pub_por_n: {
		fontWeight: 'bold',
		marginLeft: 20
	},
	pub_acciones: {
		width: '45%',
		paddingVertical: 10,
	},
	pub_acciones_sub: {
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',
	},
	pub_acciones_text: {
		textAlign: 'center',
		marginLeft: 8,
	},

	c_comentar: {
		position: 'absolute',
		width: '100%',
		height: '100%',
		top: 0,
		left: 0,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#00000088',
	},
	comentar: {
		backgroundColor: '#eee',
		width: '90%',
		// minHeight: 120,
		borderRadius: 5,
		paddingVertical: 10,
		paddingHorizontal: 20,
		flexDirection: 'row'
	},

	nueva: {
		position: 'absolute',
		bottom: 10,
		right: 10,
	}
});

const mapStateToProps = (state) => {
	return {
		user: state.user
	};
}
const mapDispatchToProps = (dispatch) => {
	return {
		getUser: (uid) => dispatch(getUser(uid))
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(Mundo));
// export default withTheme(Mundo);