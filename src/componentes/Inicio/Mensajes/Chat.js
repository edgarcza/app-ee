import React, { Component } from 'react';
import { StyleSheet, Text, View, TextInput, KeyboardAvoidingView, FlatList, TouchableHighlight, AsyncStorage } from 'react-native';
import { withTheme, IconButton } from 'react-native-paper';
import { withNavigation } from 'react-navigation';
import { Header } from 'react-navigation-stack';
import { Tema, Http } from '../../../Config';
import Firebase from '../../../Firebase'

class Chat extends Component {
	state = {
		mensajes: [
			// { id: 1, mensaje: 'Mensaje probando mensaje', tipo: 1 },
			// { id: 2, mensaje: 'Mensaje probando mensaje', tipo: 2 },
			// { id: 3, mensaje: 'Mensaje probando mensaje', tipo: 1 },
			// { id: 4, mensaje: 'Mensaje probando mensaje', tipo: 2 },
		],
		mensaje: ''
	}

	UID = null;

	constructor() {
		super();
	}

	componentDidMount() {
		// console.log(this.props.navigation.state.params)
		// AsyncStorage.getItem('uid')
		// 	.then((valor) => {
		// 		this.UID = valor;
		// 		this.CargarMensajes();
		// 	})
		// this.CargarMensajes()

		this.CargarMensajes();
		this.cm = setInterval(() => {
			this.CargarMensajes();
		}, 5000);
	}

	componentWillUnmount() {
		// this.unSub();
		clearInterval(this.cm);
	}

	async CargarMensajes() {
		// AsyncStorage.removeItem('messages')
		// return

		const { chat } = this.props.navigation.state.params;
		try {
			var RT = await AsyncStorage.getItem('rt');
			var ASMessages = await AsyncStorage.getItem('messages');
			ASJSONMessages = JSON.parse(ASMessages);
			// Si no se han guardado los mensajes o ya se guardaron pero no existen los de este chat
			if (!ASJSONMessages || (ASJSONMessages && !ASJSONMessages[chat])) {
				// Pedir todos los mensajes de este chat
				var Res = await Http.post('messages/chat', { chat: chat, rt: RT });
				// console.log(Res);

				if (!Res.error) {
					// console.log('entró');
					// if (!ASJSONMessages) ASJSONMessages = {};
					// ASJSONMessages[chat] = Res.data;
					// this.setState({ mensajes: Res.data });
					// console.log(ASJSONMessages);
					// AsyncStorage.mergeItem('messages', JSON.stringify(ASJSONMessages));
					this.AddMessages(Res.data, true);
				}
			}
			else {
				// console.log(ASJSONMessages)
				this.setState({ mensajes: ASJSONMessages[chat] });
				var Res = await Http.post('messages/chat/new', { chat: chat, rt: RT });
				if (!Res.error) {
					// ASJSONMessages[chat] = ASJSONMessages[chat].concat(Res.data);
					// this.setState({ mensajes: ASJSONMessages[chat] });
					// AsyncStorage.mergeItem('messages', JSON.stringify(ASJSONMessages));
					// console.log(ASJSONMessages[chat]);
					this.AddMessages(Res.data);
				}
			}

		}
		catch (e) {

		}
	}

	AddMessages = async (messages, set = false) => {
		const { chat } = this.props.navigation.state.params;
		var ASMessages = await AsyncStorage.getItem('messages');
		ASJSONMessages = JSON.parse(ASMessages);
		if(!ASJSONMessages) ASJSONMessages = {};
		if (set)
			ASJSONMessages[chat] = messages;
		else
			ASJSONMessages[chat] = messages.concat(ASJSONMessages[chat]);
		// ASJSONMessages[chat] = messa;
		this.setState({ mensajes: ASJSONMessages[chat] });
		AsyncStorage.mergeItem('messages', JSON.stringify(ASJSONMessages));
	}

	EnviarMensaje = async () => {
		var { mensaje, mensajes } = this.state;
		if (!mensaje) return;

		// var nMensajes = [{
		// 	message: mensaje,
		// 	type: 1,
		// 	m_state: 1,
		// 	id: parseInt((Math.random() * 100), 10)
		// }];
		// nMensajes = nMensajes.concat(mensajes);

		// this.setState({ mensajes: nMensajes, mensaje: null })
		this.setState({ mensaje: null })
		// console.log(this.state.mensajes)

		try {
			var RT = await AsyncStorage.getItem('rt');
			var Res = await Http.post('messages/chat/send', { chat: this.props.navigation.state.params.chat, rt: RT, message: mensaje });
			// console.log(Res);
			if (!Res.error)
				// this.setState({ mensajes: Res.messages });
				this.AddMessages(Res.messages, true);

		}
		catch (e) {

		}
	}

	Mensaje = (props) => {
		const { datos } = props;
		return (
			<View style={[(datos.type == 1) ? styles.contenedor_cl : styles.contenedor_sv]} onPress={() => { }}>
				<TouchableHighlight
					style={[styles.mensaje, (datos.type == 2) ? styles.mensaje_sv : styles.mensaje_cl, (datos.m_state == 1) ? styles.mensaje_unsent : null]}
					onPress={() => { }}
					underlayColor={(datos.type == 1) ? '#3867a6' : '#94afd1'}
				>
					<Text style={styles.texto}>{props.datos.message}</Text>
				</TouchableHighlight>
			</View>
		);
	}

	render() {
		const { mensajes } = this.state;
		// console.log('re render');

		return (
			<KeyboardAvoidingView
				// behavior={Platform.OS === "ios" ? "position" : null} 
				behavior='padding'
				keyboardVerticalOffset={Header.HEIGHT + 70}
				// keyboardVerticalOffset={60}
				enabled style={{ flex: 1 }}
			>
				<View style={styles.container}>
					<FlatList
						data={mensajes}
						renderItem={({ item }) => <this.Mensaje datos={item} />}
						keyExtractor={mensaje => mensaje.id.toString()}
						onEndReachedThreshold={0.1}
						// onEndReached={() => { this.CargarMensajes() }}
						inverted={true}
						extraData={this.state}
					/>
					<View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: 10, }}>
						<TextInput
							style={styles.escribir}
							value={this.state.mensaje}
							onChangeText={text => { this.setState({ mensaje: text }) }}
							placeholder="Escribe un mensaje..."
							multiline={true}
						/>
						<IconButton
							icon="send"
							color={Tema.colors.primary}
							size={30}
							onPress={() => this.EnviarMensaje()}
						/>
					</View>
				</View>
			</KeyboardAvoidingView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		paddingBottom: 10,
		// alignContent: 'flex-end',
		// alignItems: 'flex-end'
		// alignItems: 'baseline',
		// justifyContent: 'center',
	},

	contenedor_cl: {
		justifyContent: 'flex-end',
		alignItems: 'flex-end',
		alignContent: 'flex-end',
	},

	contenedor_sv: {
		// borderRadius: 30,
		// borderBottomLeftRadius: 0,
	},

	mensaje: {
		paddingVertical: 10,
		paddingHorizontal: 20,
		marginVertical: 6,
		marginHorizontal: 5,
		width: '80%',
		// borderRadius: 30,
	},

	mensaje_sv: {
		backgroundColor: '#a8c7f0',
		borderRadius: 30,
		borderBottomLeftRadius: 0,
	},

	mensaje_cl: {
		backgroundColor: Tema.colors.primary,
		borderRadius: 30,
		borderBottomRightRadius: 0,
	},

	mensaje_unsent: {
		backgroundColor: '#7e898f'
	},

	escribir: {
		width: '70%',
		// alignItems: 'center',
		justifyContent: 'center',
		// paddingVertical: 0,
		// height:,
		paddingHorizontal: 13,
		borderRadius: 20,
		borderWidth: 1,
		borderColor: Tema.colors.primary,
	},

	// contenedor_sv: {
	// 	justifyContent: 'flex-start',
	// },

	texto: {
		color: '#fff',
		margin: 0
	}
});

export default withNavigation(withTheme(Chat));