import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, FlatList, TouchableHighlight, AsyncStorage } from 'react-native';
import { withTheme, Avatar, TextInput } from 'react-native-paper';
import { withNavigation } from 'react-navigation';
import Firebase from '../../../Firebase'
import { Http } from '../../../Config';

class Mensajes extends Component {
	state = {
		mensajes: [
			// { id: 'chat' },
			// { id: 2 },
			// { id: 3 },
		],
		cargando: true,
	}

	UID = null;

	componentDidMount() {
		AsyncStorage.getItem('uid')
			.then((valor) => {
				this.UID = valor;
				this.CargarMensajes();
			})
	}

	CargarMensajes = async () => {
		this.setState({cargando: true});

		try {
			var RT = await AsyncStorage.getItem('rt');
			var Res = await Http.post('messages', {rt: RT});
			// console.log(Res);
			if(!Res.error)
				this.setState({mensajes: Res.data, cargando: false});
		} catch(e) {

		}

		// var Chats = await Firebase.firestore()
		// 	.collection('messages')
		// 	.where(`members.${this.UID}`, '==', true)
		// 	.get();
		// var chatsG = await Promise.all(Chats.docs.map(async (doc, i) => {
		// 	var chat = doc.data();
		// 	chat.id = doc.id;
		// 	var miembro = await doc.ref
		// 		.collection('members')
		// 		.doc(this.UID)
		// 		.get();
		// 	// console.log(miembro.data())
		// 	chat.avatar = miembro.data().avatar;
		// 	chat.name = miembro.data().name;
		// 	return chat;
		// }))
		// this.setState({mensajes: chatsG, cargando: false});
	}

	IrMensaje = (id) => {
		console.log('Ir Mensaje ' + id);
		this.props.navigation.navigate('Chat', { chat: id });
	}


	Mensaje = (props) => (
		<TouchableHighlight style={[styles.caja, {backgroundColor: this.props.theme.colors.background}]} onPress={() => this.IrMensaje(props.datos.message_group_id)} underlayColor='#ededed'>
			<View style={styles.mensaje}>
				<Avatar.Image style={styles.avatar} size={36} source={{uri: props.datos.mg_avatar}} />
				<View style={{ marginLeft: 10 }}>
					<Text style={[styles.de, {color: this.props.theme.colors.text}]}>{props.datos.mg_name}</Text>
					<Text style={[styles.parte, {color: this.props.theme.colors.text}]}>{`${props.datos.mg_last_message.substring(0, 30)}`}</Text>
				</View>
			</View>
		</TouchableHighlight>
	)

	constructor() {
		super();
	}

	render() {
		const { mensajes } = this.state;
		return (
			<View style={[styles.container, {backgroundColor: this.props.theme.colors.backdrop}]}>
				<FlatList
					data={mensajes}
					renderItem={({ item }) => <this.Mensaje datos={item} />}
					keyExtractor={mensaje => mensaje.id.toString()}
					onEndReachedThreshold={0.1}
					refreshing={this.state.cargando}
					onRefresh={() => { this.CargarMensajes() }}
					// onEndReached={() => { this.CargarMensajes() }}
				/>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},

	caja: {
		padding: 10,
		// borderBottomWidth: 1,
		// borderBottomColor: '#d4d4d4'
		margin: 5,
		borderRadius: 15,
		elevation: 1
	},

	mensaje: {
		flexDirection: 'row',
		alignItems: 'center'
	},
	de: {
		fontWeight: 'bold'
	},
	parte: {
		fontSize: 10,
		color: '#363636'
	}
});

export default withNavigation(withTheme(Mensajes));