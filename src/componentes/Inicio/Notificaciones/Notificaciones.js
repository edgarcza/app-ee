import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableHighlight, FlatList, AsyncStorage } from 'react-native';
// import { BottomNavigation } from 'react-native-paper';
import HeaderEE from '../../Header';
import { withTheme } from 'react-native-paper';
import { Http } from '../../../Config';

class Notificaciones extends Component {
	state = {
		notificaciones: [
			// { id: 1 },
			// { id: 2 }
		],
		cargando: true,
	}

	constructor() {
		super();
	}

	componentDidMount() {
		this.CargarNotificaciones();
	}

	CargarNotificaciones = async () => {
		// console.log('final')
		this.setState({ cargando: true });
		try {
			var RT = await AsyncStorage.getItem('rt');
			var Res = await Http.post('notifications/user', { rt: RT });
			// console.log(Res);

			if (!Res.error) {
				this.setState({ cargando: false, notificaciones: Res.data })
			}
		} catch (error) {

		}
		// this.setState({
		// 	publicaciones: [
		// 		{ id: 1 },
		// 		{ id: 2 },
		// 		{ id: 3 },
		// 		{ id: 4 },
		// 	]
		// });
	}

	Notificacion = (props) => {
		const { data } = props;
		return (
			<View style={[styles.caja, { borderColor: this.props.theme.colors.primary, backgroundColor: this.props.theme.colors.background }]}>
				<TouchableHighlight style={[styles.tcaja]} underlayColor='#ededed' onPress={() => { console.log('not') }}>
					<View>
						<View><Text style={[styles.titulo, { color: this.props.theme.colors.text }]}>{data.title}</Text></View>
						<View><Text style={[styles.descripcion, { color: this.props.theme.colors.text }]}>{data.body}</Text></View>
					</View>
				</TouchableHighlight>
			</View>)
	}

	render() {
		const { notificaciones, cargando } = this.state;
		return (
			<View style={[styles.container, { backgroundColor: this.props.theme.colors.backdrop }]}>
				<HeaderEE></HeaderEE>
				<FlatList
					data={notificaciones}
					renderItem={({ item }) => <this.Notificacion data={item} />}
					keyExtractor={not => not.id.toString()}
					onEndReachedThreshold={0.1}
					refreshing={cargando}
					onRefresh={() => this.CargarNotificaciones()}
				// onEndReached={() => { this.CargarNotificaciones() }}
				/>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		// alignItems: 'center',
		// justifyContent: 'center',
	},

	caja: {
		margin: 5,
		borderRadius: 15,
		elevation: 1,
		// borderWidth: 1,
		// borderColor: '#ddd',
		// shadowColor: "#000",
		// shadowOffset: {
		// 	width: 0,
		// 	height: 2,
		// },
		// shadowOpacity: 0.23,
		// shadowRadius: 2.62,
		// elevation: 1,
	},

	tcaja: {
		padding: 10,
	},

	titulo: {
		fontWeight: 'bold',
		fontSize: 18,
	},

	descripcion: {
		fontSize: 14,
	}
});

export default withTheme(Notificaciones);