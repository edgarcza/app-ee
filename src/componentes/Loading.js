import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, Animated } from 'react-native';
import { BottomNavigation } from 'react-native-paper';
import { withTheme } from 'react-native-paper';

class Loading extends Component {
	state = {
		loadAnim: new Animated.Value(100),
		bgAnim: new Animated.Value(1),
	}

	mostrarPrimera = true;

	constructor() {
		super();
	}

	componentWillMount() {
		// console.log(this.props.loading)
		// if(!this.props.loading)
		this.mostrarPrimera = this.props.loading;
	}

	componentDidMount() {
		this.loadTo(115);
	}

	componentWillReceiveProps(props) {
		if (this.props.loading != props.loading) {
			if (props.loading) {
				// empezó a cargar
				// console.log('a 1')
				// this.bgTo(1);
			}
			else {
				// console.log('a 0')
				this.bgTo(0);
			}
		}
	}

	bgTo = (val) => {
		Animated.timing(
			this.state.bgAnim, {
			toValue: val,
			duration: 500
		}
		).start();
	}

	loadTo = (val) => {
		Animated.timing(
			this.state.loadAnim, {
			toValue: val,
			duration: 700
		}
		).start(() => { this.loadTo(val == 115 ? 85 : 115) });
	}

	render() {
		const { loadAnim, bgAnim } = this.state;
		const { loading } = this.props;
		// if(!loading) {
		// 	console.log('no mostrar')
		// 	this.mostrarPrimera = true;
		// 	return (<View></View>)
		// }
		// console.log('mostrar')
		return (
			<Animated.View style={[styles.container, { backgroundColor: this.props.theme.colors.background, opacity: bgAnim, zIndex: loading ? 100 : 0 }]}>
				{/* <Text>{loading ? 'true' : 'false'}</Text> */}
				<Animated.Image
					style={{ width: loadAnim, height: loadAnim, resizeMode: 'contain' }}
					source={require('../../assets/LogoEE_Azul_S.png')}
				/>
			</Animated.View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		position: 'absolute',
		width: '100%',
		height: '100%',
		justifyContent: 'center',
		alignItems: 'center',
		// zIndex: 100
	},
});

export default withTheme(Loading);