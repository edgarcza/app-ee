import React, { Component } from 'react';
import * as ExpoFacebook from 'expo-facebook';
import { StyleSheet, Text, View, Image, KeyboardAvoidingView, Alert, AsyncStorage } from 'react-native';
import { TextInput, Button, IconButton, Divider } from 'react-native-paper';
import { withTheme } from 'react-native-paper';
import { withNavigation } from 'react-navigation';
import Firebase from '../Firebase';
import ByL from '../ByL';
import 'firebase/auth';
// import AsyncStorage from '@react-native-community/async-storage';
import { Facebook, Http } from '../Config'
import Icon from 'react-native-vector-icons/FontAwesome';



class Login extends Component {
  static navigationOptions = {
    headerTransparent: true,
  };
  constructor() {
    super();

    this.state = {
      email: 'ejimenez@estudiantesembajadores.com',
      // email: 'eder_josimar@hotmail.com',
      password: '123456',
      validado: true,
      iniciando: false,
    }
  }

  componentWillUnmount() {

  }

  async Login() {
    // this.props.navigation.navigate('Principal')
    // console.log(this.state);
    this.setState({ iniciando: true });

    try {
      // const LR = await Firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.password);
      // console.log(LR);
      var Log = await Http.post('user/login', { email: this.state.email, password: this.state.password });
      if (!Log.error) {
        AsyncStorage.setItem('rt', Log.data.remember_token);
        this.setState({ iniciando: false });
        this.props.navigation.navigate('Principal');
      }
      else {
        Alert.alert("Error", "Error al iniciar sesión");
      }

      // AsyncStorage.setItem('uid', LR.user.uid);
      // this.setState({iniciando: false});
      // this.props.navigation.navigate('Principal');
    } catch (error) {
      console.log(error);
      // if (error.code == 'auth/user-disabled')
      //   Alert.alert("Error", "Error");
      // else if (error.code == 'auth/invalid-email')
      //   Alert.alert("Error", "El correo no es válido");
      // else if (error.code == 'auth/user-not-found')
      //   Alert.alert('Error', "Usuario no registrado");
      // else if (error.code == 'auth/wrong-password')
      // Alert.alert("Error", "Correo o contraseña inválido");
      Alert.alert("Error", "Error al iniciar sesión");
      this.setState({ iniciando: false });
    }
  }

  // component

  async LoginFacebook() {
    const FR = await ExpoFacebook.logInWithReadPermissionsAsync(
      Facebook.id,
      { permissions: ['public_profile', 'email'] },
    );
    // console.log(FR);

    if (FR.type === 'success' && FR.token) {
      const response = await fetch(`https://graph.facebook.com/me?access_token=${FR.token}&fields=email`);
      const datos = await response.json();
      console.log(datos);
      var ByLEs = new ByL(datos.email);
      if ((await ByLEs.Buscar())) {
        // console.log('hola');
        // return;
        var Res = await Http.post('user/fb', {
          fb_token: FR.token,
          email: datos.email,
          firstName: ByLEs.estudiante.firstName,
          lastName: ByLEs.estudiante.lastName,
          name: ByLEs.estudiante.name,
          birthday: ByLEs.estudiante.birthday,
        });
        console.log(Res);
        if (!Res.error) {
          AsyncStorage.setItem('rt', Res.data.remember_token);
          this.props.navigation.navigate('Principal');
        }

      }

      // var ByLEs = new ByL(datos.email);
      // if (await ByLEs.Buscar()) {
      //   // console.log('asd');
      //   const credential = Firebase.auth.FacebookAuthProvider.credential(FR.token);
      //   try {
      //     const res = await Firebase.auth().signInWithCredential(credential);
      //     if (res.additionalUserInfo.isNewUser) {
      //       Firebase.firestore()
      //         .collection('users')
      //         .doc(res.user.uid)
      //         .set({
      //           firstName: ByLEs.estudiante.firstName,
      //           lastName: ByLEs.estudiante.lastName,
      //           name: ByLEs.estudiante.name,
      //           birthday: ByLEs.estudiante.birthday,
      //           saleId: ByLEs.saleId,
      //           avatar: 'https://i.ibb.co/dK7tCY1/1467646262-522853-1467646344-noticia-normal.jpg'
      //         });
      //     }
      //     // console.log(res.user.uid);
      //     AsyncStorage.setItem('uid', res.user.uid);
      //     this.props.navigation.navigate('Principal');
      //   } catch (error) {
      //     console.log(error);
      //   }
      // }
      else
        return alert('El correo no tiene un programa con Estudiantes Embajadores');
    }
  }

  LoginGoogle() {

  }

  Validar() {
    if (this.state.password !== "" && this.state.email !== "")
      this.setState({ validado: true });
  }

  render() {
    const { iniciando } = this.state;
    return (
      <View style={styles.container}>
        <View style={{ alignItems: 'center' }}>
          <Image
            style={{ width: '90%', height: 100 }}
            source={{ uri: 'https://i.ibb.co/Z228JLk/Logo-EE-Azul.png' }}
          />
        </View>
        <View style={{ padding: 25 }}>
          <KeyboardAvoidingView behavior='padding'>
            <View style={{ margin: 10 }}>
              <TextInput
                style={{ backgroundColor: '#fff' }}
                label='Correo electrónico'
                value={this.state.email}
                keyboardType={'email-address'}
                autoCompleteType={'email'}
                textContentType={'emailAddress'}
                autoCapitalize={'none'}
                onChangeText={text => { this.setState({ email: text }); this.Validar() }}
              />
            </View>
            <View style={{ margin: 10 }}>
              <TextInput
                style={{ backgroundColor: '#fff' }}
                label='Contraseña'
                value={this.state.password}
                autoCompleteType={'password'}
                secureTextEntry={true}
                onChangeText={text => { this.setState({ password: text }); this.Validar() }}
              />
            </View>
            <View style={{ margin: 10 }}>
              <Button
                mode="contained"
                onPress={() => { this.Login(); }}
                disabled={!this.state.validado}
                style={{ borderRadius: 40 }}
                loading={iniciando}
                disabled={iniciando}
              >
                Ingresar
            </Button>
            </View>
          </KeyboardAvoidingView>

          <Divider></Divider>

          <View style={{ margin: 10 }}>
            <Button
              style={{ marginBottom: 3, padding: 0, backgroundColor: '#0078f7', borderRadius: 40 }}
              mode="contained"
              icon={() => (<Icon name="facebook" size={24} color={'#fff'} />)}
              onPress={() => this.LoginFacebook()}
            >
              Continuar con Facebook
            </Button>
          </View>

          <View style={{ marginHorizontal: 10 }}>
            <Button
              style={{ marginBottom: 12, padding: 0, backgroundColor: '#4285f4', borderRadius: 40 }}
              mode="contained"
              icon={() => (<Icon name="google" size={20} color={'#fff'} />)}
              onPress={() => this.LoginGoogle()}
            >
              Continuar con Google
            </Button>
          </View>

          <View style={{
            borderTopWidth: 2,
            borderTopColor: this.props.theme.colors.primary,
            alignItems: 'center',
            marginTop: 20,
            marginBottom: 20,
          }}>
            <Text style={{
              marginTop: -12.5,
              backgroundColor: '#fff',
              width: 15,
              textAlign: 'center',
              fontWeight: 'bold',
              color: this.props.theme.colors.primary,
            }}>O</Text>
          </View>

          <View>
            <Button
              style={{ marginBottom: 12, padding: 0 }}
              mode="contained"
              onPress={() => this.props.navigation.navigate('Registro')}
              style={{ borderRadius: 40 }}
            >
              Registrarse
            </Button>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: '#00b7ff',
    // alignItems: 'center',
    justifyContent: 'center',
  },
});

export default withNavigation(withTheme(Login));
// export default Login;