import React, { Component } from 'react';
import { AppRegistry, StyleSheet, Text, View, AsyncStorage, ScrollView } from 'react-native';
import { ActivityIndicator } from 'react-native-paper';
import { withTheme } from 'react-native-paper';
import { connect } from 'react-redux'
import { getSales } from '../store/Actions'
import HeaderEE from './Header';
// import Moment from 'moment';
import Swiper from 'react-native-swiper'
import Firebase from '../Firebase';
import ByL from '../ByL'
import Loading from './Loading'
// import Swiper from 'react-native-swiper';

class Programas extends Component {
	Datos = [
		// {
		// 	Escuela: "UANL",
		// 	Ciudad: "Monterrey",
		// 	Programa: "Programa 1",
		// 	FechaInicio: new Date('1995,11,17'),
		// 	Seguro: {
		// 		Seguro: "Seguro 1",
		// 		Pago_Pendiente: false,
		// 		Vencimiento: "01 Enero 2020"
		// 	}
		// },
		// {
		// 	Escuela: "UN",
		// 	Ciudad: "San Nicolas",
		// 	Programa: "Programa 2",
		// 	FechaInicio: new Date('1996,11,17'),
		// 	Seguro: {
		// 		Seguro: "Seguro 2",
		// 		Pago_Pendiente: false,
		// 		Vencimiento: "01 Enero 2020"
		// 	}
		// }
	];

	state = {
		programas: [],
		cargando: false,
	}

	constructor() {
		super();
	}

	componentDidMount() {
		this.CargarProgramas();
	}

	CargarProgramas = async () => {
		// programas
		this.setState({ cargando: true });
		var rt = await AsyncStorage.getItem('rt');
		this.props.getSales(rt);
		// var User = await Firebase.firestore()
		// 	.collection('users')
		// 	.doc(UID)
		// 	.get();
		// var User = Firebase.auth().currentUser;
		// console.log(User.email)
		// console.log(User.data());
		// var Sale = await ByL.Quotes(User.data().saleId);
		// var Sales = await ByL.Programas(User.email);
		// console.log(Sale)
		// var Sales = Sale.resourceList;
		// this.setState({ programas: Sales, cargando: false });

	}

	ProgramaDato = (props) => {
		const { colors } = this.props.theme;
		return (
			<View style={[styles.programa, { backgroundColor: colors.background }]}>
				<Text style={[styles.programa_titulo, { backgroundColor: colors.background, color: colors.primary }]}>{props.titulo}</Text>
				<Text style={[styles.programa_texto]}>{props.dato}</Text>
			</View>
		)
	}

	render() {
		// Moment.locale('mx');
		// console.log(this.props.loading);
		// const { cargando } = this.state;
		const { sales, loading } = this.props;
		const { colors } = this.props.theme;
		return (
			<View style={[styles.container, { backgroundColor: colors.background }]}>
				<HeaderEE></HeaderEE>
				{/* <Text style={[styles.Programas, { backgroundColor: this.props.theme.colors.primary }]}>PROGRAMAS ESTUDIANTILES</Text> */}
				<Text style={[styles.programas2, { color: this.props.theme.colors.primary }]}>Programas</Text>
				<View style={{ borderBottomColor: "#fff", borderBottomEndRadius: 10 }}></View>

				{/* <View style={[styles.programa, { backgroundColor: colors.background }]}>
					<Text style={[styles.programa_titulo, { backgroundColor: colors.background, color: colors.primary }]}>PROGRAMA</Text>
					<Text style={[styles.programa_texto]}>Full Time Intensive Evening (33 lessons or 28 hrs/week)</Text>
				</View> */}

				<View style={{ flex: 1 }}>
					{/* {loading && (<View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}><ActivityIndicator animating={true} size={100} /></View>)} */}
					<Loading loading={loading} />
					<Swiper style={styles.wrapper} showsButtons={false} activeDotColor={colors.primary}>
						{sales.map((programa, i) => {
							const { course, institute, start, ending, lines } = programa;
							// console.log(programa);

							var startDate = new Date(start), endDate = new Date(ending);
							var startDate = `${('0' + (startDate.getDate())).slice(-2)}-${('0' + (startDate.getMonth() + 1)).slice(-2)}-${startDate.getFullYear()}`;
							var endDate = `${('0' + (endDate.getDate())).slice(-2)}-${('0' + (endDate.getMonth() + 1)).slice(-2)}-${endDate.getFullYear()}`;
							return (
								<View style={styles.slide} key={i}>
									<View style={{ alignItems: 'center', justifyContent: 'center', paddingTop: 5, paddingBottom: 12 }}>
										<Text style={{ fontWeight: 'bold', fontSize: 19, textAlign: 'center' }}>{course.name}</Text>
									</View>
									<View>
										{/* <this.ProgramaDato titulo={'PROGRAMA'} dato={course.name} /> */}
										<View style={{ flexDirection: 'row' }}>
											<this.ProgramaDato titulo={'ESCUELA'} dato={institute.name} />
											<this.ProgramaDato titulo={'CIUDAD'} dato={`${institute.city.name}, ${institute.city.country.name}`} />
										</View>
										<View style={{ flexDirection: 'row' }}>
											<this.ProgramaDato titulo={'FECHA DE INICIO'} dato={startDate} />
											<this.ProgramaDato titulo={'FECHA DE TÉRMINO'} dato={endDate} />
										</View>
									</View>

									<View style={{paddingTop: 10}}>
										<Text style={[styles.programas3, { color: this.props.theme.colors.primary }]}>Saldos</Text>
										<ScrollView style={{ paddingHorizontal: 17 }}>
											{lines.map((line, i) => {
												// console.log(line);
												return (
													<View key={i} style={[styles.saldo_box]}>
														<Text style={{fontWeight: 'bold'}}>${line.total} {line.currency}</Text>
														<Text>— {line.name}</Text>
													</View>
												)
											})}
										</ScrollView>

									</View>
									{/* <View>
										<Text style={styles.tC} style={[styles.Seguros, { color: this.props.theme.colors.primary }]}>Seguro</Text>
										<Text style={styles.tC}>Seguro: <Text style={styles.tCN}>{dato.Seguro.Seguro}</Text></Text>
										<Text style={styles.tC}>Pago <Text style={styles.tCN}>Pago_Pendiente</Text> </Text>
										<Text style={styles.tC}>Vencimiento: <Text style={styles.tCN}>asd</Text> </Text>
									</View> */}
								</View>
							)
						})}
					</Swiper>
				</View>

			</View >


			/*  
				*/


		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		paddingVertical: 10,
		alignContent: "center",
	},
	Programas: {
		textAlign: "center",
		fontSize: 27,
		paddingHorizontal: 5,
		// paddingTop: 50,
		paddingVertical: 10,
		color: '#fff',
		margin: 10,
		borderRadius: 5,
	},
	programas2: {
		fontSize: 24,
		marginLeft: 14,
		marginBottom: 5,
		fontWeight: 'bold'
	},
	programas3: {
		fontSize: 20,
		marginLeft: 14,
		marginBottom: 5,
		fontWeight: 'bold'
	},
	Seguros: {
		// textAlign: "center",
		// alignContent: "center",
		fontSize: 30,
		// paddingHorizontal: 10,
		paddingTop: 20,
		paddingBottom: 5,
	},
	tC: {
		// paddingHorizontal: 20,
		// fontSize: 16
		fontSize: 18,
		paddingVertical: 2,
	},
	tCN: {
		fontWeight: 'bold'
	},

	wrapper: {
	},
	slide: {
		flex: 1,
		// justifyContent: 'center',
		// alignItems: 'center',
		paddingTop: 10,
		paddingHorizontal: 10,
	},
	text: {
		color: '#fff',
		fontSize: 30,
		fontWeight: 'bold',
	},

	programa: {
		margin: 10,
		// borderWidth: 1,
		// borderColor: '#000',
		padding: 10,
		paddingTop: 13,
		borderRadius: 2,
		position: 'relative',
		shadowColor: '#000',
		shadowOffset: { width: 0, height: 1 },
		shadowOpacity: 0.8,
		shadowRadius: 2,
		elevation: 1,
		width: '43%'
	},
	programa_titulo: {
		position: 'absolute',
		top: -10,
		left: 10,
		fontSize: 11,
		// fontWeight: 'bold',
	},
	programa_texto: {
		fontSize: 16,
		fontWeight: 'bold'
	},
	saldo_box: {
		paddingHorizontal: 5,
		marginBottom: 10,
		paddingVertical: 4,
		borderBottomColor: '#00000033',
		borderBottomWidth: 1,
	}


});

const mapStateToProps = (state) => {
	return {
		user: state.user,
		sales: state.sales,
		loading: state.loading,
	};
}
const mapDispatchToProps = (dispatch) => {
	return {
		getSales: (email) => dispatch(getSales(email))
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(Programas));

// export default withTheme(Programas);