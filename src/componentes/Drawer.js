import React, { Component } from 'react';
import SafeAreaView from 'react-native-safe-area-view';
import { withNavigation } from 'react-navigation';
import { connect } from 'react-redux'
import { getUser } from '../store/Actions'
import Firebase from '../Firebase';
import { DrawerNavigatorItems } from 'react-navigation-drawer';
import { ScrollView, Text, View, StyleSheet, TouchableHighlight, AsyncStorage } from 'react-native';
import { IconButton, Avatar } from 'react-native-paper';
import { Tema, Http } from '../Config';

class DrawerComp extends Component {

  state = {
    usuario: {}
  }

  constructor(props) {
    super();
  }

  componentDidMount() {
    // console.log(this.props)
    this.CargarUsuario();
  }

  CargarUsuario = async () => {
    // this.UID = await AsyncStorage.getItem('uid');
    // const UsReq = await Firebase.firestore()
    // 	.collection('users')
    // 	.doc(this.UID)
    //   .get();
    // console.log(UsReq.data())
    // this.setState({usuario: UsReq.data()})
    // console.log(this.props)
  }

  componentWillReceiveProps(props) {
    // if(this.props.user != nProps.user)
    // this.setState({usuario: nProps.user})
  }

  render() {
    // console.log(this.props)
    // const { usuario } = this.state;
    const { user } = this.props;
    return (
      <ScrollView>
        <SafeAreaView
          style={{ flex: 1 }}
          forceInset={{ top: 'always', horizontal: 'never' }}
        >
          <View style={styles.cperfil}>
            <IconButton
              style={styles.salir}
              icon="exit-to-app"
              color='#fff'
              size={24}
              onPress={() => {
                // Firebase.auth().signOut();
                // AsyncStorage.getItem('rt')
                //   .then((value) => {
                //     Http.post('user/logout', {})
                //     this.props.navigation.navigate('Iniciar');
                //   })
                AsyncStorage.clear().then(() => {
                  this.props.navigation.navigate('Iniciar');
                })
                // AsyncStorage.removeItem('rt').then(() => {
                //   this.props.navigation.navigate('Iniciar');
                // })
              }}
            />
            <TouchableHighlight style={{ borderRadius: 50 }} onPress={() => { this.props.navigation.navigate('Perfil') }} underlayColor={"#ededed88"}>
              <Avatar.Image style={styles.avatar} size={100} source={{ uri: user.avatar }} />
            </TouchableHighlight>
            {/* <Text>IMAGEN</Text> */}
            {/* <Text style={styles.nombres}>{user.nombres} {user.apellidos}</Text> */}
            <Text style={styles.nombres}>{user.name}</Text>
          </View>
          <DrawerNavigatorItems {...this.props} />
        </SafeAreaView>
      </ScrollView>
    );
  }
}

// export default connect(mapStateToProps, mapDispatchToProps)(withTheme(Mundo));


// export const Drawer = props => (
//   <ScrollView>
//     <SafeAreaView
//       style={{ flex: 1 }}
//       forceInset={{ top: 'always', horizontal: 'never' }}
//     >
//       <View style={styles.cperfil}>
//         <IconButton
//           style={styles.salir}
//           icon="exit-to-app"
//           color='#fff'
//           size={24}
//           onPress={() => {
//             Firebase.auth().signOut();

//           }}
//         />
//         <Avatar.Image style={styles.avatar} size={100} source={require('../../assets/avatar.png')} />
//         {/* <Text>IMAGEN</Text> */}
//         <Text style={styles.nombres}>Nombres Apellidos</Text>
//       </View>
//       <DrawerNavigatorItems {...props} />
//     </SafeAreaView>
//   </ScrollView>
// );

const styles = StyleSheet.create({
  cperfil: {
    width: '100%',
    height: 200,
    backgroundColor: Tema.colors.primary,
    // flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingVertical: 0,
    color: 'white',
  },

  nombres: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 20
  },

  avatar: {
    marginBottom: 10,
  },

  salir: {
    position: 'absolute',
    top: 0,
    right: 0,
  }
});

const mapStateToProps = (state) => {
  return {
    user: state.user
  };
}

// export var Drawer = withNavigation(DrawerComp);
export var Drawer = connect(mapStateToProps)(withNavigation(DrawerComp));
